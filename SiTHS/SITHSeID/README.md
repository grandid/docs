# SITHS eID

## Implementation

* You will start this process by using our FederatedLogin method.
* Finally the result can be polled with GetSession.

## Requirements

* Full understanding of the functions in [GrandId API](/#a)
* A working SITHS card with either a test or production certificate
* SITHS eID client installed

## Methods

Authenticating with SITHS eID through us requires you to make two requests towards grandid.com.
The first step sends a request to SITHS eID through us, thus receiving a token which will later be used to start a session on the clients' devices. This request will be sent once and towards this url.

### Method: FederatedLogin

This references the base functionality of [FederatedLogin defined in GrandId API](/#grandid-api-methods-method-federatedlogin). Note that this should be sent as a `POST` request.

#### Request Parameters

| Request Parameters                 | Type                                | Default | Required | Comment                                                                                                                                             |
|------------------------------------|-------------------------------------|---------|----------|-----------------------------------------------------------------------------------------------------------------------------------------------------|
| apiKey                             | string                              | -       | Yes      | The customer specific key                                                                                                                           |
| authenticateServiceKey             | string                              | -       | Yes      | The service specific key                                                                                                                            |

This is what the query parameter look like `/json1.1/FederatedLogin?apiKey={apiKey}&authenticateServiceKey={authenticateServiceKey}`. See their explanation below.

#### Body parameters

| Body Parameters      | Type                                | Default | Required | Comment                                                                                                                                        |
|----------------------|-------------------------------------|---------|----------|------------------------------------------------------------------------------------------------------------------------------------------------|
| `callbackUrl`        | string, optionally base64-encoded   | -       | Yes      | URL to send user back to together with the grandidsession-parameter                                                                            |
| `personalIdentifier` | `string`                            | -       | No       | The personal identifier (PNR/SNR/HSAID) of the user. If excluded, the client must be started with the autoStartToken returned in the response. |
| `gui`                | `boolean`                           | -       | No       | Pass `false` to disable the GrandID Interface, and build your custom implementation instead.                                                   |


All parameters can be sent in the body with a POST or you can POST with the apiKey & authenticateServiceKey parameters as part of the URL. Historically callbackUrl has been sent in the URL which we strongly advise _against_. The body is sent as a standard form data.

#### Response

The GrandID API always returns a JSON response. If the request has been successfully processed you will receive the following response format:

```json
{
    "sessionId": "{sessionid}",
    "redirectUrl": "https://login.grandid.com/?sessionid={sessionid}"
}
```

##### Response with `gui=false`

If you've sent `gui=false`, or if your service is configured that way before requesting it.

You will need to either present the QR Code, or attempt to launch the user's SITHS eID application using the `autoStartToken`. For example with `siths://?autostarttoken={AUTO_START_TOKEN}&redirect={REDIRECT_URL}`

```json
{
    "sessionId": "{sessionid}",
    "QRCode": "{BASE64_ENCODED_QR_CODE_IMAGE}",
    "autoStartToken": "{AUTO_START_TOKEN}"
}
```

### Method: GetSession

This references the base functionality of [GetSession defined in GrandId API](/#grandid-api-methods-method-getsession).

Currently, there are no differences between SITHS eID and our generic API.

#### Example response SITHS eID
```json
{
  "sessionId": "7648379ada38568512ea0f363ee5733d",
  "username": "MY_USERNAME",
  "userAttributes": {
    "name": "/C=SE/O=SOME_ORGANIZATION/CN=MY_COMMON_NAME/SN=MY_SUR_NAME/GN=MY_GIVEN_NAME/serialNumber=MY_USERNAME",
    "serialNumber": "SERIAL_NUMBER",
    "issuer": {
      "C": "SE",
      "O": "Inera AB",
      "CN": "TEST SITHS e-id Person ID Mobile CA v1"
    },
    "subject": {
      "C": "SE",
      "O": "SOME_ORGANIZATION",
      "CN": "MY_COMMON_NAME",
      "SN": "MY_SUR_NAME",
      "GN": "MY_GIVEN_NAME",
      "serialNumber": "MY_USERNAME"
    },
    "validFrom": 1665142295,
    "validFromHumanReadable": "2022-10-07T11:31:35+00:00",
    "validTo": 1728293495,
    "validToHumanReadable": "2024-10-07T09:31:35+00:00",
    "signatureTypeSN": "RSA-SHA256",
    "signatureTypeLN": "sha256WithRSAEncryption"
  }
}
```

#### Example responses with `gui=false`

When you're building a custom interface implementation the responses from the _GetSession_ endpoint changes during the polling.

The completed session will produce the same response however, seen above.

##### Successfully polling status

When you're polling the status of the transaction, you'll receive a response like below.

```json
{
    "grandidObject": {
      "code": "SITHSEID_MSG",
      "sessionId": "{SessionId}",
      "QRCode": "{BASE64_ENCODED_QR_CODE_IMAGE}",
      "autoStartToken": "{AUTO_START_TOKEN}"
    }
}
```

##### Error polling status

If something went wrong, you'll receive the following error object.

```json
{
    "errorObject": {
      "code": "SITHSEID_MSG",
      "message": "{error_message}"
    }
}
```

### Method: Logout

This references the base functionality of [Logout defined in GrandId API](/#grandid-api-methods-method-logout).

Currently, there are no differences between SITHS eID and our generic API.

## Errors

This references the base information of [Errors defined in GrandId API](/#grandid-api-errors).

Currently, there are no differences between SITHS eID and our generic API.


## Examples

In all the examples below, that send API calls against the production environment (`client.grandid.com`), you can switch to the test environment by changing the domain name to `client-test.grandid.com'.

### Authentication

> SITHS eID Authentication
> In order to initiate a SITHS eID authentication you can just send a POST request to our FederatedLogin endpoint with your `apiKey` and `authenticateServiceKey`.

| Request parameter                  | Type                                | Value (Default)                    | Required | Comment                                                                                                                                  |
|------------------------------------|-------------------------------------|------------------------------------|----------|------------------------------------------------------------------------------------------------------------------------------------------|
| Content-Type                       | header                              | `application/x-www-form-urlencoded`  | Yes      | The type of request being made                                                                                                           |
| Endpoint                           | The service answering               | `https://client.grandid.com/json1.1/` | Yes      | The URL of the request (concatenated with Method below)                                                                                  |
| Method                             | The function being called           | `FederatedLogin`                     | Yes      | GrandID have several methods, most commonly FederatedLogin, GetSession and Logout                                                        |
| apiKey                             | string, GET/BODY                    | `your apiKey`                        | Yes      | The customer specific key                                                                                                                |
| authenticateServiceKey             | string, GET/BODY                    | `your authenticateServiceKey`        | Yes      | The service specific key                                                                                                                 |
| callbackUrl                        | string, BODY                        | `your callback url`                  | Yes      | It is possible to send the callbackUrl as a parameter directly in the URL, but it's something we strongly advise against                 |

#### CURL Authentication Example

```bash
curl --request POST \
  --url 'https://client.grandid.com/json1.1/FederatedLogin?apiKey=MY_API_KEY&authenticateServiceKey=MY_AUTHENTICATE_SERVICE_KEY' \
  --header 'content-type: application/x-www-form-urlencoded' \
  --data callbackUrl=https://localhost/example
```

#### Authentication response

```json
{
    "sessionId": "{sessionid}",
    "redirectUrl": "{redirectUrl}"
}
```
