# Net iD Enterprise

## Implementation

The implementation of Net iD Enterprise (SiTHS) uses a client software called "Net iD Enterprise". When the user has this software installed, they can expose their certificate information to our IDP using two way TLS.
To achieve this functionality you must initiate FederatedLogin, return the users browser to the returned redirectUrl and finally retrieve the user information back from us using GetSession.

## Requirements

- Full understanding of the functions in [GrandId API](/#a)
- A working SiTHS card with either a test or production certificate
- [Net iD Enterprise installed](https://e-identitet.se/netid)

## Services

A quick rundown of the differences between the HSA ID and PersonID services. If you are unsure which of these applies to you it is likely *SITHS HSA ID*.

From a technical point of view there is no major differences between these services. The API behaves the same way. The main difference is which type of service the keys you have is configured for, and also the [GetSession](/netID_enterprise#net-id-enterprise-methods-method-getsession) response information.

### SITHS HSA ID

HSA ID is a professional identity, related to the user's workplace. This means aside from having a name tied to them there isn't necessarily any additional private information tied to this identity.

### SITHS PersonID

PersonID is a personal identity, which is related to a person's private information. With PersonID you can get access to the user's personal number (YYYYMMDDXXXX). You can see a visualization of the authentication flow for this service by [clicking here](https://cdn.grandid.com/docs/siths_personid_flow.png).

### Telia eID

The Telia eID is an electronic identity, which is the parent for both SITHS HSA ID and SITHS PersonID.

It can be used for authentications with the services in a similar manner. There are no particular precautions you need to take into account.

You can build your implementation according to the documentation below.

## Methods

Authenticating with Net iD through us requires you to make two requests towards grandid.com.
The first step sends a request to bankid through us, thus receiving a token which will later be used to start a session on the clients' devices. This request will be sent once and towards this url

### Method: FederatedLogin

This references the base functionality of [FederatedLogin defined in GrandId API](/#grandid-api-methods-method-federatedlogin).

Currently there are no differences between Net iD Enterprise and our generic API.

### Method: GetSession

This references the base functionality of [GetSession defined in GrandId API](/#grandid-api-methods-method-getsession).

Currently there are no differences between Net iD Enterprise and our generic API.

#### Example response Net iD Enterprise HSA ID

```json
{
    "sessionId": "203eb192b622a50cc44087e03f58fc98",
    "username": "SE123456789-9876",
    "userAttributes": {
        "serialNumber": "SE123456789-9876",
        "lastname": "Lastname",
        "firstname": "Firstname",
        "clientCertificateSerial": "88780.....52357",
        "DN_Email": "user@domain.com"
    }
}
```

#### Example response Net iD Enterprise PersonID

```json
{
    "sessionId": "203eb192b622a50cc44087e03f58fc98",
    "username": "YYYYMMDDXXXX",
    "userAttributes": {
        "serialNumber": "YYYYMMDDXXXX",
        "lastname": "Lastname",
        "firstname": "Firstname",
        "clientCertificateSerial": "88780...52357",
        "DN_Email": []
    }
}
```

### Method: Logout

This references the base functionality of [Logout defined in GrandId API](/#grandid-api-methods-method-logout).

Currently there are no differences between Net iD Enterprise and our generic API.

## Errors

This references the base information of [Errors defined in GrandId API](/#grandid-api-errors).

Currently there are no differences between Net iD Enterprise and our generic API.

## Examples

The examples are written with the production environment in mind (`login.grandid.com` and `client.grandid.com`).  Use `login-test.grandid.com` and `client-test.grandid.com` for the test environment.

### Authentication

> Net iD Enterprise Authentication:
> To start a Net iD Enterprise session you need to authenticate with GrandID

| Request parameter      | Type                      | Value (Default)                    | Required | Comment                                                                                                                  |
| ---------------------- | ------------------------- | ---------------------------------- | -------- | ------------------------------------------------------------------------------------------------------------------------ |
| Content-Type           | header                    | application/x-www-form-urlencoded  | Yes      | The type of request being made                                                                                           |
| Endpoint               | The service answering     | [https://login.grandid.com/json1.1/](https://login.grandid.com/json1.1/) | Yes      | The URL of the request (concatenated with Method below)                                                                  |
| Method                 | The function being called | FederatedLogin                     | Yes      | GrandID have several methods, most commonly FederatedLogin, GetSession and Logout                                        |
| apiKey                 | string, GET/BODY          | your apiKey                        | Yes      | The customer specific key                                                                                                |
| authenticateServiceKey | string, GET/BODY          | your authenticateServiceKey        | Yes      | The service specific key                                                                                                 |
| callbackUrl            | string, BODY              | your callback url                  | Yes      | It is possible to send the callbackUrl as a parameter directly in the URL, but it's something we strongly advise against |

#### CURL Example

```bash
curl --request POST \
  --url 'https://client.grandid.com/json1.1/FederatedLogin?apiKey=apiKey&authenticateServiceKey=authenticateServiceKey' \
  --header 'content-type: application/x-www-form-urlencoded' \
  --data callbackUrl=https://localhost/example
```

#### Response

```json
{
    "sessionId": "{sessionid}",
    "redirectUrl": "{redirectUrl}"
}
```
