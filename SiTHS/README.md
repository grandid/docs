# SiTHS

## Description

SiTHS is a physical card which can be used by private and public entities in Sweden. It's a PKI-based technology primarily used by health care. Svensk e-identitet offers a suite of services around SiTHS

## Overview

![SiTHS overview architecture](/assets/SiTHS_infra.png "SiTHS overview for a authentication")

## Services from Svensk e-identitet

* Authentication via SiTHS cards using TLS & [Net iD Enterprise](/netID_enterprise).
* Mobile access via SiTHS cards and the appropriate hardware. A full PKI solution using the [Net iD Access Server & Net iD Access client](/netidaccess).
* You can also use [SITHS eID Client](/SITHSeID).
* Full administrative support for [management of the distribution process of cards](https://e-identitet.se/auth/ombud/) to end users (Ombud). Contact ombud@e-identitet.se for more information about this service.

If you require cards with test certificates, Ombud can help you with this.
