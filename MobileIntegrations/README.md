# Mobile integrations

This documentation is meant to inform you about how to build your application to work together with the GrandID API, and will assume that you already have a good understanding of the [GrandID API documentation](/).

## Requirements

- You **MUST** have a good understanding of the [GrandID API documentation](/).
- You **SHOULD** have a good understanding of the documentation for the service you're integrating with, such as [BankID](/bankid) or [Net iD Access](/netidaccess).
- Any requests directly towards the GrandID API **MUST NOT** be done from directly within the app.
- The `FederatedLogin` and `GetSession` requests **MUST** be done from your backend, not within the client's application.

## Webview

When you load a GrandID service through a webview you leave almost all of the logic up to GrandID. This means there is less hassle for you to deal with. However there are certain requirements for this to work.

### Webview requirements

- The client **MUST** have a session URL on grandid.com, like `https://login.grandid.com?sessionid={SessionId}`.
- The application **MUST** have access to the internet, otherwise it will not be able to connect to login.grandid.com.
- JavaScript execution **MUST** be enabled in the webview, as the GrandID relies on JavaScript.
- The useragent in the webview **SHOULD** be unique, otherwise it may cause unintended behavior due to incorrect flagging of webbrowser and operating system. If not please see the relevant section on redirecting the client back to your application.

### Custom URL Schemes

GrandID might need to start an authentication application, such as BankID or Net iD Access, in which the user must authenticate. These applications may be started automatically by GrandID. You as a developer **SHOULD** take this into account. Some applications may be started by trying to navigate to a custom URL scheme. This is known to be able to cause issues with the webview trying to navigate and display these pages, but instead rendering an error screen. As a developer you should catch the navigation, and if it starts with a known custom URL scheme you should instead try to launch the relevant app. Luckily this is a fairly straight forward error that is easy to spot. The Webview will navigate to a page and show an error such as `ERR_UNKNOWN_URL_SCHEME`.

### Redirecting back to your application

After the user has identified in the authentication application they **SHOULD** be redirected back to your webview application. Most GrandID interfaces try to do this, but it may not be possible due to restrictions in the operating system. Instead we have a parameter that you can use to show the application to the user. This parameter is passed with the `FederatedLogin` request and is usually named `appRedirect`. See the service's specific documentation to see how to do this for the service you're integrating with.

#### BankID example logic

Example logic to prevent the `ERR_UNKNOWN_URL_SCHEME` error. This detection **MUST** be done before the webview actually starts loading the new "page". If it is done at an "onUrlChange"-callback, it may be too late. See the example flow below.

1. Detect if the new requested URL uses the `bankid` URL scheme.
2. Prevent the webview from navigating to `bankid:///?autostarttoken=[token]&redirect=[redirect]`.
3. Try to start the BankID application on the device instead.

## Custom applications

When building apps based on GrandID's "No GUI" or "For apps" services a lot more responsibility is left to the application's developer. The developer **MUST** have a good understanding of the specific GrandID documentation for the service they're integrating with. Below are the **important parts of the application** that you as a developer will need to take into account while developing your application and communicating with the [GrandID API](/).

### Starting a session

You will need to run a `FederatedLogin` request towards the GrandID API in order to retrieve a token, which might be necessary to launch the authentication application, such as BankID, on the client's device. However in the case of multiple people sharing the same device you are unlikely to be able to launch an application on the same device and might instead expected the user to authenticate on a separate device. In that case you will instead need to pass some kind of user identifier to the `FederatedLogin` endpoint. See the service's documentation on how to do this.

> **IMPORTANT**: these requests **MUST** be done through a backend server and **MUST NOT** be done by the client's application.

### Polling status

The status of the authentication or signature **SHOULD** be polled regularly according to the service's documentation. Restrictions in how often it should be polled can be found in each service's documentation. This is usually done in intervals of a couple of seconds.

### User authentication

> This section is irrelevant if the user is expected to authenticate on a separate device.

The user will need to authenticate in some way, usually by entering a PIN code into a separate application (such as [BankID](/bankid)). This section describes the flow of launching a separate authentication application, and handling the return from said application.

#### Launching authentication application

When you have sufficient information from the user to start their authentication application, you can launch that application on their device. What information is required will vary depending on the services, but common information is their personal number or a start token.

#### Redirecting client back to your application

When launching the application it is common for the authentication application to accept some sort of redirect parameter, defining how or the operating system or application should redirect the user back to where they came from. This will vary depending on operating systems and the unique authentication applications. You may for example need to either pass a URL, or `null`. See the service's specific documentation for more information on this.

This behaviour can sometimes be achived by the appRedirect parameter in FederatedLogin or manually in your application (not for BankiD where you need to manually construct your bankid-launch URI in this context).

### Verifying successful authentication

When the polling has completed you **MUST** verify that the user has been successfully logged in by verifying with a `GetSession` request. This can be handled directly within the polling loop. While the polling may have completed, this does not necessarily mean that the user has successfully authenticated. It may mean that they failed to authenticate within the given timeout, they intetionally cancelled the authentication or something else. This is something that needs to be accounted for while parsing the response from the `GetSession` request.

It is also important to undestand that while the user may have successfully authenticated, they may still not have sufficient privileges to access a requested resource. This is something that GrandID cannot account for and it is up to you to ensure that the user with their attributes that were returned from `GetSession` are allowed to access the requested resource.

> **IMPORTANT**: these requests **MUST** be done through a backend server and **MUST NOT** be done by the client's application.

### Generic illustration of flow

You can see a generic example flow in the image below.

![GrandID Mobile Integration](/img/grandid-mobile-integrations.svg "A generic flow of how the client application interacts with different systems to authenticate the user.")
