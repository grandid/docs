# Integration Broker

## What it means for us
To serve as an integration broker means that we act as a sort of middle point between user organisations and services. 
Since we can communicate with most well-known protocols we can save our customers from wasting their valuable time on this type of system integration by doing it for them.

## What it means for our customers
As stated above, our customers don't want to spend their time fiddling with IDPs to work in their scenarios. The goal is to make it as easy for them as possible to come and ask for our help, we will then solve these integrations ourselves.

Read more at https://e-identitet.se/auth/integrationsmetoder/integrationsbroker/

![IB example image](https://e-identitet.se/content/uploads/2020/12/integrationsbroker-illustration1-900x506.png)

