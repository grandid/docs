# EFOS

## Implementation

The implementation of Net iD Access (EFOS) is an infrastructure much similar to the one being used in BankiD.
When initiating a login or signing request we communicate with a central server which is hosted by Svensk e-identitet.
Using the correct configuration in the Net iD Access client (mobile or desktop) the clients device will know that a authentication or signature was initiated.

![GrandID NiAS / Net iD Access](https://cdn.grandid.com/docs/grandid_flow.png "GrandID overview for a authentication")

In the above picture, you can just switch out "BankID" with "Net iD Access Server".

* You will start this process by using our FederatedLogin method.
* Communicating ensues between the clients device, the NiAS server and our backend.
* Finally the result can be polled with GetSession.

## Requirements

* Full understanding of the functions in [GrandId API](/#a)
* A working EFOS card with either a test or production certificate
* [Net iD Access installed](https://e-identitet.se/netid)

## Methods

Authenticating with Net iD through us requires you to make two requests towards grandid.com.
The first step sends a request to Net iD Access through us, thus receiving a token which will later be used to start a session on the clients' devices. This request will be sent once and towards this url.

### Method: FederatedLogin

This references the base functionality of [FederatedLogin defined in GrandId API](/#grandid-api-methods-method-federatedlogin). Note that this should be sent as a `POST` request.

#### Request Parameters

| Request Parameters                 | Type                                | Default | Required | Comment                                                                                                                                             |
|------------------------------------|-------------------------------------|---------|----------|-----------------------------------------------------------------------------------------------------------------------------------------------------|
| apiKey                             | string                              | -       | Yes      | The customer specific key                                                                                                                           |
| authenticateServiceKey             | string                              | -       | Yes      | The service specific key                                                                                                                            |

This is what the query parameter look like `/json1.1/FederatedLogin?apiKey={apiKey}&authenticateServiceKey={authenticateServiceKey}`. See their explanation below.

#### Body parameters

| Body Parameters                    | Type                                | Default | Required | Comment                                                                                                                                             |
|------------------------------------|-------------------------------------|---------|----------|-----------------------------------------------------------------------------------------------------------------------------------------------------|
| callbackUrl                        | string, optionally base64-encoded   | -       | Yes      | URL to send user back to together with the grandidsession-parameter                                                                                 |
| userVisibleData                    | string, base64-encoded              | -       | No       | Mandatory (if signing) data to sign and show to the user. MUST be Base64 encoded.                                                                   |
| userNonVisibleData                 | string, base64-encoded              | -       | No       | Optional data to sign which is not shown (otherwise userVisibleData is copied to this parameter). MUST be Base64 encoded.                           |
| personalNumber                     | string                              | -       | -        | Optional user identifier, HSA-ID to initiate an authentication for that user.  See signature section for more details. |
| thisDevice                         | boolean                                | -       | No       | Force whether the user must authenticate/sign on their current device.                                                                              |
| appRedirect                        | string, URL- and UTF8-encoded       | -       | No       | Controls how the user is redirected back after identifying in Net iD Access. See [Mobile Integrations documentation](/mobileintegrations).          |
| gui                                | boolean                                | true    | No       | If set to false we return a autoStartToken instead of redirectUrl on FederatedLogin and does not require our ui to be used.                                |


All parameters can be sent in the body with a POST or you can POST with the apiKey & authenticateServiceKey parameters as part of the URL. Historically callbackUrl has been sent in the URL which we strongly advise _against_. The body is sent as a standard form data.

#### Response

The GrandID API always returns a JSON response. If the request has been successfully processed you will receive the following response format:

```json
{
    "sessionId": "{sessionid}",
    "redirectUrl": "https://login.grandid.com/?sessionid={sessionid}"
}
```

There are cases where the redirectUrl is replaced with  "**autoStartToken**" for example when the gui parameter is set to ** *false* ** and instead of the url you will receive a net id access token which you can use to start the users' net id access app directly from the client side.

> Example: netid:///?autoStartToken={autoStartToken}&redirect=null

The second step involves you using this session id to collect the information of that user's authentication.

### Method: GetSession

This references the base functionality of [GetSession defined in GrandId API](/#grandid-api-methods-method-getsession).

Currently there are no differences between Net iD Access service and our generic API.

#### Example response Net iD Access
```json
{
  "sessionId": "dcfb4472c6dac328f3bd125a3d1f8eb2",
  "username": "SE123456789-9876",
  "userAttributes": {
    "name": "Firstname Lastname",
    "personalNumber": "SE123456789-9876",
    "notBefore": "10/16/2015 7:55:28 AM UTC",
    "notAfter": "10/14/2020 9:57:59 PM UTC",
    "givenName": "Firstname",
    "surname": "Lastname",
    "certificate": "MIIGzTCCBLW... certificate data ...WzTGkpU=",
    "signature": "MIIJvgY... signature data ...kYLJU="
  }
}
```

### Method: Logout

This references the base functionality of [Logout defined in GrandId API](/#grandid-api-methods-method-logout).

Currently there are no differences between the Net iD Access service and our generic API.

## Errors

This references the base information of [Errors defined in GrandId API](/#grandid-api-errors).

Except for the error response below ( in that case, you should contact support@e-identitet.se ), there are no other differences in comparison to our generic api 

```json
{
    "errorObject": {
        "message": "Invalid service combination. Must use GrandID interface with Commissions",
        "code": "INVALID_CONFIGURATION_COMMISSIONS"
    }
}
```



## Examples

In all the examples below, that send API calls against the production environment (`client.grandid.com`), you can switch to the test environment by changing the domain name to `client-test.grandid.com'.

### Authentication

> Net iD Access Authentication
> In order to initiate a Net iD Access authentication you can just send a POST request to our FederatedLogin endpoint with your `apiKey` and `authenticateServiceKey`.

| Request parameter                  | Type                                | Value (Default)                    | Required | Comment                                                                                                                                  |
|------------------------------------|-------------------------------------|------------------------------------|----------|------------------------------------------------------------------------------------------------------------------------------------------|
| Content-Type                       | header                              | `application/x-www-form-urlencoded`  | Yes      | The type of request being made                                                                                                           |
| Endpoint                           | The service answering               | `https://client.grandid.com/json1.1/` | Yes      | The URL of the request (concatenated with Method below)                                                                                  |
| Method                             | The function being called           | `FederatedLogin`                     | Yes      | GrandID have several methods, most commonly FederatedLogin, GetSession and Logout                                                        |
| apiKey                             | string, GET/BODY                    | `your apiKey`                        | Yes      | The customer specific key                                                                                                                |
| authenticateServiceKey             | string, GET/BODY                    | `your authenticateServiceKey`        | Yes      | The service specific key                                                                                                                 |
| callbackUrl                        | string, BODY                        | `your callback url`                  | Yes      | It is possible to send the callbackUrl as a parameter directly in the URL, but it's something we strongly advise against                 |

#### CURL Authentication Example

```bash
curl --request POST \
  --url 'https://client.grandid.com/json1.1/FederatedLogin?apiKey=MY_API_KEY&authenticateServiceKey=MY_AUTHENTICATE_SERVICE_KEY' \
  --header 'content-type: application/x-www-form-urlencoded' \
  --data callbackUrl=https://localhost/example
```

#### Authentication response

```json
{
    "sessionId": "{sessionid}",
    "redirectUrl": "{redirectUrl}"
}
```

### Signature (visible)

> Net iD Access Signatures
> In order to initiate a Net iD Access signature you MUST pass either the `userVisibleData` or `userNonVisibleData` parameter in the body of the request. These values MUST be Base64 encoded.

| Request parameter                  | Type                                | Value                              | Required | Comment                                                                                                                                  |
|------------------------------------|-------------------------------------|------------------------------------|----------|------------------------------------------------------------------------------------------------------------------------------------------|
| Content-Type                       | header                              | `application/x-www-form-urlencoded`  | Yes      | The type of request being made                                                                                                           |
| Endpoint                           | The service answering               | `https://client.grandid.com/json1.1/` | Yes      | The URL of the request (concatenated with Method below)                                                                                  |
| Method                             | The function being called           | `FederatedLogin`                     | Yes      | GrandID have several methods, most commonly FederatedLogin, GetSession and Logout                                                        |
| apiKey                             | string, GET/BODY                    | `your apiKey`                        | Yes      | The customer specific key                                                                                                                |
| authenticateServiceKey             | string, GET/BODY                    | `your authenticateServiceKey`        | Yes      | The service specific key                                                                                                                 |
| personalNumber                     | string                              | `SEDDDDDDDD-DDDD`                    | Yes      | Required user identifier, HSA-ID, to initiate an authentication/signature for that user.  |
| callbackUrl                        | string, BODY                        | `your callbackUrl`                   | Yes      | Your callback URL for signing requests                                                                                                   |
| userVisibleData                    | string, BODY, base64 encoded        | `base64('TEST')`                     | Yes      | A base64 encoded string which contains the string to show users when signing. Setting this value enables signing functionality           |
| userNonVisibleData                 | string, BODY, base64 encoded        | `base64('TEST')`                     | No       | A base64 encoded string which contains hidden data for the user to sign. Setting this value enables signing functionality                |

#### CURL Signature Example

```bash
curl --request POST \
  --url 'https://client.grandid.com/json1.1/FederatedLogin?apiKey=MY_API_KEY&authenticateServiceKey=MY_AUTHENTICATE_SERVICE_KEY' \
  --header 'content-type: application/x-www-form-urlencoded' \
  --data userVisibleData=VEVTVA== \
  --data callbackUrl=https://localhost/example
```

#### Signature Response

Just like with authentication you will be returned a redirectUrl to send the user to.

```json
{
    "sessionId": "{sessionid}",
    "redirectUrl": "{redirectUrl}"
}
```

When the user is sent back your callbackUrl after successfully signing, you retrieve the results via GetSession just like a normal login.

### Signature (hidden)

> Net iD Access Signatures
> To start a Net iD Access sign session our API needs a string to sign. This string MUST be base64 encoded.

| Request parameter                  | Type                                | Value                              | Required | Comment                                                                                                                                  |
|------------------------------------|-------------------------------------|------------------------------------|----------|------------------------------------------------------------------------------------------------------------------------------------------|
| Content-Type                       | header                              | `application/x-www-form-urlencoded`  | Yes      | The type of request being made                                                                                                           |
| Endpoint                           | The service answering               | `https://client.grandid.com/json1.1/` | Yes      | The URL of the request (concatenated with Method below)                                                                                  |
| Method                             | The function being called           | `FederatedLogin`                     | Yes      | GrandID have several methods, most commonly FederatedLogin, GetSession and Logout                                                        |
| apiKey                             | string, GET/BODY                    | `your apiKey`                        | Yes      | The customer specific key                                                                                                                |
| authenticateServiceKey             | string, GET/BODY                    | `your authenticateServiceKey`        | Yes      | The service specific key                                                                                                                 |
| callbackUrl                        | string, BODY                        | `your callbackUrl`                   | Yes      | Your callback URL for signing requests                                                                                                   |
| userVisibleData                    | string, base64                      | `base64('TEST')`                     | Yes      | A base64 encoded string which contains the string to show users when signing. Setting this value enables signing functionality           |
| userNonVisibleData                 | string, base64                      | `base64('TEST')`                     | No       | A base64 encoded string which contains the string to sign. If this does not exist, userVisibleData is copied to this value by GrandID    |

#### CURL Hidden Signature Example

```bash
curl --request POST \
  --url 'https://client.grandid.com/json1.1/FederatedLogin?apiKey=apiKey&authenticateServiceKey=authenticateServiceKey' \
  --header 'content-type: application/x-www-form-urlencoded' \
  --data userNonVisibleData=VEVTVA== \
  --data callbackUrl=https://localhost/example \
```

#### Hidden Signature Response

Just like with authentication you will be returned a redirectUrl to send the user to.

```json
{
    "sessionId": "{sessionid}",
    "redirectUrl": "{redirectUrl}"
}
```

When the user is sent back your callbackUrl after successfully signing, you retrieve the results via GetSession just like a normal login.

### Redirect to app

> In order to redirect the user back to a specific application after they authenticate or sign in Net iD Access you may pass the `appRedirect` parameter to the `FederatedLogin` endpoint.

| Request parameter                  | Type                                | Value (Default)                      | Required | Comment                                                                                                                                  |
|------------------------------------|-------------------------------------|--------------------------------------|----------|------------------------------------------------------------------------------------------------------------------------------------------|
| Content-Type                       | header                              | `application/x-www-form-urlencoded`  | Yes      | The type of request being made                                                                                                           |
| Endpoint                           | The service answering               | `https://client.grandid.com/json1.1/` | Yes      | The URL of the request (concatenated with Method below)                                                                                  |
| Method                             | The function being called           | `FederatedLogin`                     | Yes      | GrandID have several methods, most commonly FederatedLogin, GetSession and Logout                                                        |
| apiKey                             | string, GET/BODY                    | `your apiKey`                        | Yes      | The customer specific key                                                                                                                |
| authenticateServiceKey             | string, GET/BODY                    | `your authenticateServiceKey`        | Yes      | The service specific key                                                                                                                 |
| callbackUrl                        | string, BODY                        | `your callback url`                  | Yes      | It is possible to send the callbackUrl as a parameter directly in the URL, but it's something we strongly advise against                 |
| appRedirect                        | string, BODY                        | `the app redirect url`               | Yes      | You can pass the `appRedirect` to the Net iD Access application to force how it redirects the user after identifying.                    |

#### CURL Redirect to Google Chrome application

```bash
curl --request POST \
  --url 'https://client.grandid.com/json1.1/FederatedLogin?apiKey=MY_API_KEY&authenticateServiceKey=MY_AUTHENTICATE_SERVICE_KEY' \
  --header 'content-type: application/x-www-form-urlencoded' \
  --data appRedirect=googlechrome%3A%2F%2Fwww.example.org \
  --data callbackUrl=https://localhost/example \
```

### Net iD Access for apps

![GrandID for apps architecture](https://cdn.grandid.com/docs/grandid_backend_for_apps.png "GrandID overview for a authentication with a for apps solution")


> Net iD for apps
> You receive a netid start token, which then you can redirect your user to the netid app on their device


| Request parameter                  | Type                                | Value                              | Required | Comment                                                                                                                                  |
|------------------------------------|-------------------------------------|------------------------------------|----------|------------------------------------------------------------------------------------------------------------------------------------------|
| Content-Type                       | header                              | application/x-www-form-urlencoded  | Yes      | The type of request being made                                                                                                           |
| Endpoint                           | The service answering               | https://client.grandid.com/json1.1/ | Yes      | The URL of the request (concatenated with Method below)                                                                                  |
| Method                             | The function being called           | FederatedLogin                     | Yes      | GrandID have several methods, most commonly FederatedLogin, GetSession and Logout                                                        |
| apiKey                             | string, GET/BODY                    | your apiKey                        | Yes      | The customer specific key                                                                                                                |
| authenticateServiceKey             | string, GET/BODY                    | your authenticateServiceKey        | Yes      | The service specific key                                                                                                                 |
| gui                                | boolean                             | false                              | Yes      | Setting GUI to false disables the GrandID UI and instead lets you create one

To use this functionality you need to implement your own Net iD Access UI on your on website or app. Instead of a redirectUrl you will be returned an autoStartToken. If you are building a mobile application you should read the [Mobile Integrations documentation](/mobileintegrations).

**CURL Example**

```bash
curl --request POST \
  --url 'https://client.grandid.com/json1.1/FederatedLogin?apiKey=apiKey&authenticateServiceKey=authenticateServiceKey' \
  --header 'content-type: application/x-www-form-urlencoded' \
  --data gui=false \
```

#### Successful response

```json
{
    "sessionId": "{sessionid}",
    "autoStartToken": "{autoStartToken from Net iD Access}"
}
```

You will need the autoStartToken to start Net iD Access on the user device. By redirecting the user to the following url `netid:///?autoStartToken={autoStartToken}` the user should be prompted to launch the Net iD access application. In order to get continuous status updates, you should poll GetSession continuosly in the background in some manner. You will be returned the current status of the login process which you can relay to the user. When a user succesfully completes the confirmation process, the user attributes are returned. **This polling cannot exceed 1 request every other second**.

When using gui=false you can also send a parameter personalNumber (HSA-iD) with the request to force an authentication (or sign) on a different device. If you want the user to be redirected back to your application or a specific url, then you should set the redirect parameter together with the autoStartToken when you launch Net iD Access. `netid:///?autoStartToken={autoStartToken}&redirect=yourapp://.`

#### FederatedLogin error response

Below is the generic error format for the FederatedLogin request.

```json
{
  "errorObject": {
    "code": "SOME_ERROR_CODE",
    "message": "Some error message."
  }
}
```

##### Invalid HSA ID

When the HSA ID passed in the `personalNumber` parameter is not valid, you get the following response. Note that this differs from an unknown user, which might still be formatted as a valid HSA ID, but not a known ID.

```json
{
  "errorObject": {
    "code": "INVALID_HSA_ID",
    "message": "The HSA-ID you have sent is invalid."
  }
}
```

##### Unknown user

The passed user identifier is not known.

```json
{
  "errorObject": {
    "message": "The End User is unknown or not valid.",
    "code": "UNKNOWN_USER"
  }
}
```

#### GetSession Responses

When polling the GetSession endpoint, you will receive responses similar to these.

##### Generic format

The generic format, note that the values are not actual values that are passed.

```json
{
  "statusObject": {
    "code": "SOME_STATUS_CODE",
    "message": "Some status message."
  }
}
```

##### Outstanding transaction

The transaction has started, but the user hasn't started their Net iD Access software yet.

```json
{
  "statusObject": {
    "code": "NETID_ACCESS_MESSAGE",
    "message": "OUTSTANDING_TRANSACTION"
  }
}
```

##### User sign

User has opened their Net iD Access software, and have been prompted for their PIN code.

```json
{
  "statusObject": {
    "code": "NETID_ACCESS_MESSAGE",
    "message": "USER_SIGN"
  }
}
```

##### Cancelled transaction

The transaction was cancelled, likely due to taking too long. You should **NOT** continue polling after this.

```json
{
  "statusObject": {
    "code": "NETID_ACCESS_MESSAGE",
    "message": "CANCELLED"
  }
}
```

##### Completed session

When the authentication is completed the API responds according to [Method: GetSession](/netidaccess#net-id-access-methods-method-getsession).

## Special Cases

When running Net iD Access + Enterprise services ( which are a combination of both Net iD Access and Net iD Enterprise as the name suggests ), the api will return different attributes depending on the technique that is used to login.
If Net iD Access is chosen to login, then it will return the same attributes as stated in our documentation. However, Net iD Enterprise will return a different output:

```json
{
  "sessionId": "{sessionid}",
  "username": "SEXXXXXXXXXXX-XXXX",
  "userAttributes": {
    "clientCertificateSerial": "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
    "DN_Email": [],
    "personalNumber": "SEXXXXXXXXXXX-XXXX",
    "surname": "Efternamn",
    "givenName": "Namn"
  }
}
```
The difference is that personalNumber, surname, givenName are the same with Net iD Access, the rest of the attributes persist from Net iD Enterprise. Certificate and signature are not provided as there is none in Enterprise
