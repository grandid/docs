# Net iD Enterprise

## Implementation

The implementation of EFOS uses a client software called "Net iD Enterprise". When the user has this software installed, they can expose their certificate information to our IDP using two way TLS.
To achieve this functionality you must initiate FederatedLogin, return the users browser to the returned redirectUrl and finally retrieve the user information back from us using GetSession.

## Requirements

- Full understanding of the functions in [GrandId API](/#a)
- A working EFOS card with either a test or production certificate
- [Net iD Enterprise installed](https://e-identitet.se/netid)

## Methods

Authenticating with Net iD Enterprise through us requires you to make two requests towards grandid.com.
The first step is the FederatedLogin request which instructs you to redirect your end-user to a `redirectUrl`.
That is where GrandID takes over and handles the authentication with Net iD Enterprise on its own side.
GrandID will challenge the end user for their EFOS Card using a 2-way SSL Authentication method.
The user is prompted to choose the certificate they want to use to authenticate, then are redirected to the callbackUrl.

### Acceptable client certificate Certificate Authorities names
```
/C=SE/O=Swedish Social Insurance Agency/CN=Swedish Public Sector Customer CA v1
/C=SE/O=Swedish Social Insurance Agency/CN=Swedish Public Sector RPA CA v2
/C=SE/O=Swedish Social Insurance Agency/CN=Swedish Public Sector Mobile ID CA v1
/C=SE/O=Swedish Social Insurance Agency/CN=Swedish Public Sector Person 2 CA v1
/C=SE/O=Swedish Social Insurance Agency/CN=Swedish Public Sector Person 3 CA v1
/C=SE/O=Swedish Social Insurance Agency/CN=Swedish Public Sector Person 4 CA v1
```

### Method: FederatedLogin

This references the base functionality of [FederatedLogin defined in GrandId API](/#grandid-api-methods-method-federatedlogin).

Currently there are no differences between Net iD Enterprise and our generic API.

### Method: GetSession

This references the base functionality of [GetSession defined in GrandId API](/#grandid-api-methods-method-getsession).

Currently there are no differences between Net iD Enterprise and our generic API.

#### Example response Net iD Enterprise HSA ID

```json
{
  "sessionId": "203eb192b622a50cc44087e03f58fc98",
  "username": "SE123456789-9876",
  "userAttributes": {
    "serialNumber": "SE123456789-9876",
    "lastname": "Lastname",
    "firstname": "Firstname",
    "clientCertificateSerial": "88780.....52357",
    "DN_Email": "user@domain.com"
  }
}
```

#### Example response Net iD Enterprise PersonID

```json
{
    "sessionId": "203eb192b622a50cc44087e03f58fc98",
    "username": "YYYYMMDDXXXX",
    "userAttributes": {
        "serialNumber": "YYYYMMDDXXXX",
        "lastname": "Lastname",
        "firstname": "Firstname",
        "clientCertificateSerial": "88780...52357",
        "DN_Email": []
    }
}
```

### Method: Logout

This references the base functionality of [Logout defined in GrandId API](/#grandid-api-methods-method-logout).

Currently there are no differences between Net iD Enterprise and our generic API.

## Errors

This references the base information of [Errors defined in GrandId API](/#grandid-api-errors).

Currently there are no differences between Net iD Enterprise and our generic API.

## Examples

The examples are written with the production environment in mind (`login.grandid.com` and `client.grandid.com`).  Use `login-test.grandid.com` and `client-test.grandid.com` for the test environment.

### Authentication

> Net iD Enterprise Authentication:
> To start a Net iD Enterprise session you need to authenticate with GrandID

| Request parameter      | Type                      | Value (Default)                    | Required | Comment                                                                                                                  |
| ---------------------- | ------------------------- | ---------------------------------- | -------- | ------------------------------------------------------------------------------------------------------------------------ |
| Content-Type           | header                    | application/x-www-form-urlencoded  | Yes      | The type of request being made                                                                                           |
| Endpoint               | The service answering     | [https://login.grandid.com/json1.1/](https://login.grandid.com/json1.1/) | Yes      | The URL of the request (concatenated with Method below)                                                                  |
| Method                 | The function being called | FederatedLogin                     | Yes      | GrandID have several methods, most commonly FederatedLogin, GetSession and Logout                                        |
| apiKey                 | string, GET/BODY          | your apiKey                        | Yes      | The customer specific key                                                                                                |
| authenticateServiceKey | string, GET/BODY          | your authenticateServiceKey        | Yes      | The service specific key                                                                                                 |
| callbackUrl            | string, BODY              | your callback url                  | Yes      | It is possible to send the callbackUrl as a parameter directly in the URL, but it's something we strongly advise against |

#### CURL Example

```bash
curl --request POST \
  --url 'https://client.grandid.com/json1.1/FederatedLogin?apiKey=apiKey&authenticateServiceKey=authenticateServiceKey' \
  --header 'content-type: application/x-www-form-urlencoded' \
  --data callbackUrl=https://localhost/example
```

#### Response

```json
{
  "sessionId": "{sessionid}",
  "redirectUrl": "{redirectUrl}"
}
```
