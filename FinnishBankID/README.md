# Finnish BankID

## Implementation

We strongly recommend our customers to NOT use Finnish BankID in an iFrame solution.

## Requirements

- Full understanding of the functions in [GrandId API](/#a)
- A working Finnish BankID on a desktop computer or mobile device. You can get a free test key including instructions for installing test certificates in the menu above.

## Methods

Authenticating with Finnish BankID through us requires you to make two requests towards grandid.com.
We forward the user via SAML to the Finnish BankID IDP and handles the SAML messaging if desired.

### Method: FederatedLogin

This references the base functionality of [FederatedLogin defined in GrandId API](/#grandid-api-methods-method-federatedlogin).

Currently there are no differences between Finnish BankID and our generic API.
### Method: GetSession

This references the base functionality of [GetSession defined in GrandId API](/#grandid-api-methods-method-getsession).

Currently there are no differences between Finnish BankID and our generic API.

[GET]

| Request Parameters                 | Type                                | Default | Required | Comment                                                                                                                                  |
|------------------------------------|-------------------------------------|---------|----------|------------------------------------------------------------------------------------------------------------------------------------------|
| apiKey                             | string                              | -       | Yes      | The customer specific key                                                                                                                |
| authenticateServiceKey             | string                              | -       | Yes      | The service specific key                                                                                                                 |
| sessionId                          | string                              | -       | Yes      | Session id returned from FederatedLogin or sent to the callbackUrl endpoint in "grandidsession" via GET                                  |

Just like with FederatedLogin, GetSession accepts these values to be sent both in the URL or in the POST BODY. This method's parameters can all successfully sent in the URL.

**Example BankID Auth/Sign GetSession**

```json
{
    "sessionId": "1baa7e7cc4ddb3b7c33318e909b3886b",
    "username": "010200A9618",
    "userAttributes": {
        "family_name": "Korhonen",
        "given_name": "Onni Juhani",
        "ssn": "010200A9618"
    }
}
```

**DISCLAIMER**
The same results are not applicable if a standard service is not used or a standard service is modified  .
The social security number displayed is not real.
Result generated on (2021-05-26).

#### Example response Finnish BankID

```json
{
     "family_name": "MEIKÄLÄINEN",
     "given_name": "MAIJA",
     "ssn":"750240-1228"
}
```

| Response parameter                              | Value                      |
| ----------------------------------------------- | -------------------------- |
| ​fi_tupas                                        | AuthenticationID           |
| IDType: SSN / IDValue: The SSN value            | ​SignerID                   |
| ​abs:fi_tupas                                    | forcepkivendor             |

### Method: Logout

This references the base functionality of [Logout defined in GrandId API](/#grandid-api-methods-method-logout).

Currently there are no differences between Finnish BankID and our generic API.

## Errors

This references the base information of [Errors defined in GrandId API](/#grandid-api-errors).

Currently there are no differences between Finnish BankID and our generic API.

## Examples

The examples are written with the production environment in mind (`login.grandid.com` and `client.grandid.com`).  Use `login-test.grandid.com` and `client-test.grandid.com` for the test environment.

### Authentication

> Finnish BankID Authentication:
> To start a Finnish BankID session you need to authenticate with GrandID

| Request parameter      | Type                      | Value (Default)                    | Required | Comment                                                                                                                  |
| ---------------------- | ------------------------- | ---------------------------------- | -------- | ------------------------------------------------------------------------------------------------------------------------ |
| Content-Type           | header                    | application/x-www-form-urlencoded  | Yes      | The type of request being made                                                                                           |
| Endpoint               | The service answering     | [https://login.grandid.com/json1.1/](https://login.grandid.com/json1.1/) | Yes      | The URL of the request (concatenated with Method below)                                                                  |
| Method                 | The function being called | FederatedLogin                     | Yes      | GrandID have several methods, most commonly FederatedLogin, GetSession and Logout                                        |
| apiKey                 | string, GET/BODY          | your apiKey                        | Yes      | The customer specific key                                                                                                |
| authenticateServiceKey | string, GET/BODY          | your authenticateServiceKey        | Yes      | The service specific key                                                                                                 |
| callbackUrl            | string, BODY              | your callback url                  | Yes      | It is possible to send the callbackUrl as a parameter directly in the URL, but it's something we strongly advise against |

#### CURL Example

```bash
curl --request POST \
  --url 'https://client.grandid.com/json1.1/FederatedLogin?apiKey=apiKey&authenticateServiceKey=authenticateServiceKey' \
  --header 'content-type: application/x-www-form-urlencoded' \
  --data callbackUrl=https://localhost/example
```

#### Response

```json
{
    "sessionId": "{sessionid}",
    "redirectUrl": "{redirectUrl}"
}
```
