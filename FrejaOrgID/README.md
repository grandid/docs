# Freja Organisation eID

## Requirements

-   Full understanding of the functions in [GrandId API](/#a)
-   A working Freja ([test](https://frejaeid.com/testing/Freja%20eID%20Relying%20Party%20Documentation%20-%20Testing%20instructions.html#FrejaeIDRelyingPartyDocumentation-Testinginstructions-StartingtheFrejaeIDMobileApplicationinTestMode) or production) installation on your mobile device
-   Basic understanding of [Freja eID API](/freja)

## Methods

Authenticating with Freja through us requires you to make two requests towards grandid.com.
The first step sends a request to Freja through us, thus receiving a token which will later be used to start a session on the clients' devices. This request will be sent once and towards this url

### Method: FederatedLogin

See [Freja eID API Documentation](/freja#freja-methods-method-federatedlogin)

### Method: GetSession

See [Freja eID API Documentation](/freja#freja-methods-method-getsession)

## Services

### Manage

Ordinarily users are managed manually, by authenticating into a user interface. Removal of users can be automated by calling our API, with the appropriate `authenticateServiceKey` and `apiKey`, according to below.

#### Automatic User Removal

To automate removal of users, call "FederatedLogin" with these extra parameters:

| Parameter        | Description                                        |
|------------------|----------------------------------------------------|
| `action`         | This parameter should always equal to "RemoveUser" |
| `company`        | Represents the organisations title                 |
| `userIdentifier` | Represents username                                |


When calling FederatedLogin with these parameters, no redirectUri or session id will be returned. The API will instead respond with 2 attributes: success and message.

| Attribute | Description |
|-----------|-------------|
| `success` | bool        |
| `message` | string      |




Example:

```bash
curl --request GET \
  --url 'https://client.grandid.com/json1.1/FederatedLogin?apiKey=[redacted]&authenticateServiceKey=[redacted]&action=RemoveUser&company=Organisation-title&userIdentifier=username'
```

### Pair

Freja OrgId Pair must be called separately by the customer using the same procedure used by logins.
Please read more in [GrandId API](/#a) and [Freja eID API Documentation](/#freja)

### Auth

Authentications works the same way as with Freja eID 

See [Freja eID API Documentation](/freja#freja-examples-personal-number-login)

### Sign

Signatures works the same way as with Freja eID

See [Freja eID API Documentation](/freja#freja-examples-signing)

### List Freja Organization eID Organization and Users

It is also possible to get the list of users in json format using our list organizations and organization users api service.

The api works in the same way as the services above and has two actions.

You have to call the `FederatedLogin` endpoint with the following parameters:

| Parameter                | Description                                                                           |
|--------------------------|---------------------------------------------------------------------------------------|
| `apiKey`                 | The secret key that was given to you by us.                                           |
| `authenticateServiceKey` | The secret key that is unique for each pair of keys.                                  |
| `action`                 | One of: `ListFrejaOrganizations`, or `ListFrejaOrganizationIds`.                      |
| `organizationId`         | Required only when action=`ListFrejaOrganizationIds`.                                 |
| `page`                   | Optional. The page number when the number of items is bigger than max items per page. |
| `length`                 | Optional. The number of items per page (max: 100).                                    |

The response will be different from the services above.


The response structure when action is set to `ListOrganizations`:

```json
{
	"current_page": integer,
	"data": [
		{
			"id": "string",
			"created_at": "2023-05-26 10:05:28",
			"updated_at": "2023-05-26 10:05:28",
			"data": {
				"organisationId": {
					"title": "string"
				},
				"frejaOrganization": bool
			}
		}
	],
	"from": integer,
	"last_page": integer,
	"per_page": integer
}
```

The response structure when action is set to `ListOrganizationIds`:

```json
{
  "current_page": integer,
  "data": [
    {
      "id": "string",
      "user_id": "string",
      "organization_id": "string",
      "data": {
        "expiry": "string",
        "initAdd": {
          "orgIdRef": "string"
        },
        "userInfo": "string",
        "userInfoType": "string",
        "organisationId": {
          "title": "string",
          "identifier": "string",
          "identifierName": "string"
        },
        "userIdentifier": "string",
        "minRegistrationLevel": "string"
      },
      "created_at": "2020-06-02 12:04:07",
      "updated_at": "2020-06-02 12:04:07"
    },
    ...
  ],
  "from": integer,
  "last_page": integer,
  "per_page": integer
}
```

This does not have that many functions therefore there are only 2 cases when it can throw an error: 

1. When `action` is set to something other than the allowed values.
2. When `organizationId` parameter is missing when action=`ListOrganizationIds`.

Error example: 

```json
{
    "errorObject": {
        "code": 400,
        "message": "Missing required parameter: organizationId"
    }
}
```

#### Examples

```shell
curl --request POST \
  --url 'https://client.grandid.com/json1.1/FederatedLogin' \
  --header 'Content-Type: multipart/form-data' \
  --form apiKey=[API KEY] \
  --form authenticateServiceKey=[SERVICE KEY] \
  --form action=ListFrejaOrganizations
```

```shell
curl --request POST \
  --url 'https://client.grandid.com/json1.1/FederatedLogin' \
  --header 'Content-Type: multipart/form-data' \
  --form apiKey=[API KEY] \
  --form authenticateServiceKey=[SERVICE KEY] \
  --form action=ListFrejaOrganizationIds \
  --form organizationId=[ID OF ORGANIZATION FETCHED FROM THE PREVIOUS ENDPOINT]
```