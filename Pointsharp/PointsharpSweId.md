# Pointsharp SweID

## Implementation

The implementation of Pointsharp SweID using the GrandID TLS solution requires the use of a suitable application, such as Net Id Client in order to retrieve the certificates from your hardware.

This certificate is then transferred to the GrandID host to perform a mutual TLS authentication. After which the end-user's client is returned to the specified callbackUrl.

## Requirements

- Full understanding of the functions in [GrandId API](/#a)
- A suitable token with a Pointsharp SweID certificate
- A suitable PKI software, such as Net Id Client.

## Methods

Authenticating with Net iD through us requires you to make two requests towards grandid.com, first `FederatedLogin` then `GetSession`.

### Method: FederatedLogin

This references the base functionality of [FederatedLogin defined in GrandId API](/#grandid-api-methods-method-federatedlogin).

Currently, there are no differences for this service compared to the basic GrandID API implementation.

### Method: GetSession

This references the base functionality of [GetSession defined in GrandId API](/#grandid-api-methods-method-getsession).

Currently, there are no differences for this service compared to the basic GrandID API implementation.

#### Example response

An example response including the attributes.

```json
{
    "sessionId": "{GRANDID_SESSION_ID}",
    "username": "{SOME_USERNAME}",
    "userAttributes": {
        "serialNumber": "YYYYMMDDXXXX",
        "lastname": "string",
        "firstname": "string",
        "clientCertificateSerial": "string"
    }
}
```

### Method: Logout

This references the base functionality of [Logout defined in GrandId API](/#grandid-api-methods-method-logout).

Currently, there are no differences for this service compared to the basic GrandID API implementation.

## Errors

This references the base information of [Errors defined in GrandId API](/#grandid-api-errors).

Currently, there are no differences for this service compared to the basic GrandID API implementation.

## Examples

The examples are written with the production environment in mind (`login.grandid.com` and `client.grandid.com`).  Use `login.test.grandid.com` and `client.test.grandid.com` for the test environment.

### Authentication

| Request parameter      | Type                      | Value (Default)                    | Required | Comment                                                                                                                  |
| ---------------------- | ------------------------- | ---------------------------------- | -------- | ------------------------------------------------------------------------------------------------------------------------ |
| Content-Type           | header                    | application/x-www-form-urlencoded  | Yes      | The type of request being made                                                                                           |
| Endpoint               | The service answering     | [https://login.grandid.com/json1.1/](https://login.grandid.com/json1.1/) | Yes      | The URL of the request (concatenated with Method below)                                                                  |
| Method                 | The function being called | FederatedLogin                     | Yes      | GrandID have several methods, most commonly FederatedLogin, GetSession and Logout                                        |
| apiKey                 | string, GET/BODY          | your apiKey                        | Yes      | The customer specific key                                                                                                |
| authenticateServiceKey | string, GET/BODY          | your authenticateServiceKey        | Yes      | The service specific key                                                                                                 |
| callbackUrl            | string, BODY              | your callback url                  | Yes      | It is possible to send the callbackUrl as a parameter directly in the URL, but it's something we strongly advise against |

#### CURL Example

```bash
curl --request POST \
  --url 'https://client.grandid.com/json1.1/FederatedLogin?apiKey=apiKey&authenticateServiceKey=authenticateServiceKey' \
  --header 'content-type: application/x-www-form-urlencoded' \
  --data callbackUrl=https://localhost/example
```

#### Response

```json
{
    "sessionId": "{sessionid}",
    "redirectUrl": "{redirectUrl}"
}
```
