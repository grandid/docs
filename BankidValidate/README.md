# BankID Digital ID Card Verification Service

This document describes our BankID Digital ID Card Verification service and how to integrate it using our API. By leveraging this service, you can verify user attributes provided via scanned QR codes from Digital ID Cards.

# Overview


## Required parameters

These are the required parameters

## API Key

- Name: `apiKey`
- Required? **Yes**
- Type: `string`

## Authenticate Service Key

- Name: `authenticateServiceKey`
- Required: **Yes**
- Type: `string`

## QR Code

- Name: `qrCode`
- Required: **Yes**
- Type: `string`

The scanned qr code data from the BankID digital ID card.

# Example Curl Request

Below you can see an example cURL request including every single parameter.

## Request

```shell
curl --request POST \
  --url https://client.grandid.com/json1.1/FederatedLogin \
  --header 'Content-Type: multipart/form-data' \
  --form apiKey={APIKEY} \
  --form authenticateServiceKey={AUTHENTICATESERVICEKEY}\
  --form qrCode={QRCODE}
```

# Response examples

## Success Response
**Status Code:** `200 OK`  
This indicates the user verification was successful, and the session data is returned.

```json
{
  "sessionId": "<SESSION_ID>",
  "username": "189001029819",
  "userAttributes": {
    "personalNumber": "189001029819",
    "name": "John Doe",
    "givenName": "John",
    "surname": "Doe"
  }
}
```

## Error Responses

### **Missing QR Code:**
   When the `qrCode` parameter is not provided.
   **Status Code**: `400 Bad Request`

   ```json
   {
     "errorObject": {
       "code": 400,
       "message": "Missing required parameter: `qrCode`"
     }
   }
   ```

### **Verification Failed:**
   This error indicates an issue during ID card verification.
   **Status Code**: `500 Internal Server Error`

   ```json
   {
     "errorObject": {
       "code": 500,
       "message": "Failed to verify id card"
     },
     "sessionId": "<SESSION_ID>"
   }
   ```

### **BankID API Error:**
   If a specific error occurs from the BankID API.
   **Status Code**: `400 Bad Request`

   ```json
   {
     "errorObject": {
       "code": "<ERROR_CODE>",
       "message": "<ERROR_MESSAGE>"
     },
     "sessionId": "<SESSION_ID>"
   }
   ```

---


For additional information or support, please refer to the [official BankID API documentation](https://developers.bankid.com), or contact our support team.

---