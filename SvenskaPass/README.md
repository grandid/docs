# Svenska Pass

## Implementation

Svenska Pass provided by Thales Group is a Swedish e-legitimation based on smart card technology...

## Requirements

- Full understanding of the functions in [GrandId API](/#a)
- A working Svenska Pass card with either a test or production certificate and the corresponding card reader

### Svenska Pass

In certain situations it is desirable to have a Svenskt Pass instead of a Bankid or Freja...

## Methods

Authenticating with Svenska Pass through us requires you to make two requests towards grandid.com.
We forward the user via SAML to the Svenska Pass IDP and handles the SAML messaging if desired.

### Method: FederatedLogin

This references the base functionality of [FederatedLogin defined in GrandId API](/#grandid-api-methods-method-federatedlogin).

Currently there are no differences between Svenska Pass and our generic API.

### Method: GetSession

This references the base functionality of [GetSession defined in GrandId API](/#grandid-api-methods-method-getsession).

Currently there are no differences between Svenska Pass and our generic API.

#### Example response Svenska Pass

```json
 {
  "sessionId": "044a3640964f2f84f75a96f9cdbf3677",
  "username": "193101189227",
  "userAttributes": {
    "urn:oid:1.2.752.29.4.13": "193101189227",
    "urn:oid:2.5.4.42": "Anna",
    "urn:oid:2.5.4.4": "Sjöström",
    "urn:oid:2.16.840.1.113730.3.1.241": "Anna Sjöström",
    "urn:oid:1.3.6.1.5.5.7.9.3": "F",
    "urn:oid:1.3.6.1.5.5.7.9.1": "1931-01-18",
    "urn:oid:2.5.4.6": "SE",
    "urn:oid:1.2.752.34.2.1": "9752272696700015699",
    "username": "6017fbbba41dc"
  }
}
```

| Response parameter                              | Value                      |
| ----------------------------------------------- | -------------------------- |
| urn:oid:1.2.752.29.4.13                         | User personal number       |
| urn:oid:2.5.4.42                                | First name                 |
| urn:oid:2.5.4.4                                 | Last name                  |
| urn:oid:2.16.840.1.113730.3.1.241               | Full name                  |
| urn:oid:1.3.6.1.5.5.7.9.3                       | Gender (M/F/U)             |
| urn:oid:1.3.6.1.5.5.7.9.1                       | Date of birth              |
| urn:oid:2.5.4.6                                 | Country name               |
| urn:oid:1.2.752.34.2.1                          | Certificate serial number  |

### Method: Logout

This references the base functionality of [Logout defined in GrandId API](/#grandid-api-methods-method-logout).

Currently there are no differences between Svenska Pass and our generic API.

## Errors

This references the base information of [Errors defined in GrandId API](/#grandid-api-errors).

Currently there are no differences between Svenska Pass and our generic API.

## Examples

The examples are written with the production environment in mind (`login.grandid.com` and `client.grandid.com`).  Use `login-test.grandid.com` and `client-test.grandid.com` for the test environment.

### Authentication

> Svenska Pass Authentication:
> To start a Svenska Pass session you need to authenticate with GrandID

| Request parameter      | Type                      | Value (Default)                    | Required | Comment                                                                                                                  |
| ---------------------- | ------------------------- | ---------------------------------- | -------- | ------------------------------------------------------------------------------------------------------------------------ |
| Content-Type           | header                    | application/x-www-form-urlencoded  | Yes      | The type of request being made                                                                                           |
| Endpoint               | The service answering     | [https://login.grandid.com/json1.1/](https://login.grandid.com/json1.1/) | Yes      | The URL of the request (concatenated with Method below)                                                                  |
| Method                 | The function being called | FederatedLogin                     | Yes      | GrandID have several methods, most commonly FederatedLogin, GetSession and Logout                                        |
| apiKey                 | string, GET/BODY          | your apiKey                        | Yes      | The customer specific key                                                                                                |
| authenticateServiceKey | string, GET/BODY          | your authenticateServiceKey        | Yes      | The service specific key                                                                                                 |
| callbackUrl            | string, BODY              | your callback url                  | Yes      | It is possible to send the callbackUrl as a parameter directly in the URL, but it's something we strongly advise against |

#### CURL Example

```bash
curl --request POST \
  --url 'https://client.grandid.com/json1.1/FederatedLogin?apiKey=apiKey&authenticateServiceKey=authenticateServiceKey' \
  --header 'content-type: application/x-www-form-urlencoded' \
  --data callbackUrl=https://localhost/example
```

#### Response

```json
{
    "sessionId": "{sessionid}",
    "redirectUrl": "{redirectUrl}"
}
```
