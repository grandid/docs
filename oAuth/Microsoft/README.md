# Microsoft IDP

## Implementation

The implementation of Microsoft login requires the same knowledge about GrandID API as every other service.
This service allows you to authenticate using Microsoft accounts such as Microsoft Live or Office 365.
This works similarly to other social IDP's such as Google/Gmail, Twitter, Facebook and others.

![GrandID / Microsoft IDP](https://cdn.grandid.com/docs/grandid_flow.png "GrandID overview for a authentication")

In the above picture, you can just switch out "BankID" with "Microsoft".

* You will start this process by using our FederatedLogin method.
* Communicating ensues between the client's device, Microsoft IDP and our backend.
* Finally the result can be fetched with GetSession.

### Microsoft Accounts

This service supports both private Microsoft Live accounts and business accounts for Office 365.

Office 365 accounts can be an account that is created by your business or school, intended for your personal use in your work or school environment.
The private accounts in Microsoft Live could be your private account in hotmail or outlook, intended for your private use outside of work or school.


## Requirements

* Full understanding of the functions in [GrandId API](/#a)
* An active Microsoft account (Private or Organisation/Student)

## Methods

Authenticating with Microsoft through us requires you to make two requests towards grandid.com.
The first step sends a request to Microsoft through us, thus receiving a token which will later be used to start a session on the client's devices. This request will be sent once and towards this url.
The client must accept that we gain access to information (generally the user only needs to accept this the first time) about the account. This can affect users using office 365 account which might need an admin's permission to hand out information.
The second step is fetching the user's attributes from us by performing a GetSession request when the client navigates to the callbackUrl you have specified in the FederatedLogin request.

### Method: FederatedLogin

This references the base functionality of [FederatedLogin defined in GrandId API](/#grandid-api-methods-method-federatedlogin). Note that this should be sent as a `POST` request.

#### Endpoint

| Environment     | Domain name                  |
|-----------------|------------------------------|
| Production (EU) | `client.grandid.com`         |
| Test (EU)       | `client-test.grandid.com`    |
| Production (SE) | `client.e-identitet.se`      |
| Test (EU)       | `client-test.e-identitet.se` |

#### Request Parameters

| Request Parameters                 | Type                                | Default | Required | Comment                                                                                                                                             |
|------------------------------------|-------------------------------------|---------|----------|-----------------------------------------------------------------------------------------------------------------------------------------------------|
| apiKey                             | string                              | -       | Yes      | The customer specific key                                                                                                                           |
| authenticateServiceKey             | string                              | -       | Yes      | The service specific key                                                                                                                            |

This is what the query parameter look like `/json1.1/FederatedLogin?apiKey={apiKey}&authenticateServiceKey={authenticateServiceKey}`. See their explanation below.

#### Response

The GrandID API always returns a JSON response. If the request has been successfully processed you will receive the following response format:

```json
{
  "sessionId": "{sessionid}",
  "redirectUrl": "https://login.grandid.com?sessionid={sessionid}"
}
```

### Method: GetSession

This references the base functionality of [GetSession defined in GrandId API](/#grandid-api-methods-method-getsession).

Currently there are no differences between Microsoft IDP and our generic API.

#### Example response Microsoft Live
```json
{
  "sessionId": "{sessionid}",
  "username": "email@example.org",
  "userAttributes": {
    "name": "Firstname Lastname",
    "preferred_username": "email@example.org",
    "email": "email@example.org"
     }
}
```

#### Example response Microsoft Office 365
```json
{
  "sessionId": "{sessionid}",
  "username": "email@example.org",
  "userAttributes": {
    "family_name": "Services",
    "given_name": "Federation",
    "name": "Federation",
    "unique_name": "email@example.org",
    "upn": "email@example.org"
  }
}
```

### Method: Logout

This references the base functionality of [Logout defined in GrandId API](/#grandid-api-methods-method-logout).

Currently there are no differences between Microsoft and our generic API.

## Errors

This references the base information of [Errors defined in GrandId API](/#grandid-api-errors).

Currently there are no differences between Microsoft and our generic API except the following error:

```
  An error occured
  Please try logging in again. Make sure you select the correct account type.
```

This is an error that the user will encounter and that most likely means that you chose the wrong account type.

## Examples

The FederatedLogin request for Microsoft Live and Microsoft Office services are identical.

The examples below use the domain name `client.grandid.com` in the production environment, but it is also possible to use the test environment, the corresponding domain name is `client-test.grandid.com`.

### Microsoft Login example

> Microsoft login
> In order to initiate a Microsoft login you can just send a POST request to our FederatedLogin endpoint with your `apiKey` and `authenticateServiceKey`.

| Request parameter                  | Type                                | Value (Default)                    | Required | Comment                                                                                                                                  |
|------------------------------------|-------------------------------------|------------------------------------|----------|------------------------------------------------------------------------------------------------------------------------------------------|
| Content-Type                       | header                              | `application/x-www-form-urlencoded`  | Yes      | The type of request being made                                                                                                           |
| Endpoint                           | The service answering               | `https://client.grandid.com/json1.1/` | Yes      | The URL of the request (concatenated with Method below)                                                                                  |
| Method                             | The function being called           | `FederatedLogin`                     | Yes      | GrandID have several methods, most commonly FederatedLogin, GetSession and Logout                                                        |
| apiKey                             | string, GET/BODY                    | `your apiKey`                        | Yes      | The customer specific key                                                                                                                |
| authenticateServiceKey             | string, GET/BODY                    | `your authenticateServiceKey`        | Yes      | The service specific key                                                                                                                 |
| callbackUrl                        | string, BODY                        | `your callback url`                  | Yes      | It is possible to send the callbackUrl as a parameter directly in the URL, but it's something we strongly advise against                 |

#### CURL Microsoft login Example

```bash
curl --request POST \
  --url 'https://client.grandid.com/json1.1/FederatedLogin?apiKey=MY_API_KEY&authenticateServiceKey=MY_AUTHENTICATE_SERVICE_KEY' \
  --header 'content-type: application/x-www-form-urlencoded' \
  --data callbackUrl=https://localhost/example
```

#### Microsoft login Response

```json
{
  "sessionId": "{sessionid}",
  "redirectUrl": "https://login.grandid.com?sessionid={sessionid}"
}
```
