# oAuth

We have chosen to highlight our solutions for [Google](/google) and [Microsoft](/microsoft) oAuth IDPs, but any data source which is oAuth compatible can be used. Contact info@e-identitet.se for more information.
## Implementation

Our implementation of oAuth & OpenID Connect allows you to easily integrate such IDPS in your solution using our simple REST API.
This solution can also be used via SAML, where Svensk E-identitet acts as a proxy instead of GrandID API if that is desirable.

* You will start this process by using our FederatedLogin method.
* Communicating ensues between the client's device, the IDP and our backend.
* Finally the result can be fetched with GetSession.

### Social media using oAuth or OpenID Connect

Svensk e-identitet will simplify your connection to these different data sources and user catalogues. This service can be used if you wish to allow users from a certain social media network access to your service or if you have exposed your user database with oAuth.

## Requirements

* Full understanding of the functions in [GrandId API](/#a)
* A configurable oAuth IDP where our credentials can be added. 

### Method: FederatedLogin

This references the base functionality of [FederatedLogin defined in GrandId API](/#grandid-api-methods-method-federatedlogin). Note that this should be sent as a `POST` request.

#### Request Parameters

| Request Parameters                 | Type                                | Default | Required | Comment                                                                                                                                             |
|------------------------------------|-------------------------------------|---------|----------|-----------------------------------------------------------------------------------------------------------------------------------------------------|
| apiKey                             | string                              | -       | Yes      | The customer specific key                                                                                                                           |
| authenticateServiceKey             | string                              | -       | Yes      | The service specific key                                                                                                                            |

This is what the query parameter look like `/json1.1/FederatedLogin?apiKey={apiKey}&authenticateServiceKey={authenticateServiceKey}`. See their explanation below.

#### Response

The GrandID API always returns a JSON response. If the request has been successfully processed you will receive the following response format:

```json
{
  "sessionId": "{sessionid}",
  "redirectUrl": "https://login.grandid.com?sessionid={sessionid}"
}
```

### Method: GetSession

This references the base functionality of [GetSession defined in GrandId API](/#grandid-api-methods-method-getsession).

Depending on the attributes exposed by the IDP this list will differ.

#### Example response from a Google IDP
```json
{
  "sessionId": "{sessionid}",
  "username": "{email}",
  "userAttributes": {
    "iss": "https://accounts.google.com",
    "azp": "{id}.apps.googleusercontent.com",
    "aud": "{id}.apps.googleusercontent.com",
    "sub": "{sub}",
    "hd": "{domain}",
    "email": "{email}",
    "email_verified": true,
    "at_hash": "{hash}",
    "name": "{full name}",
    "picture": "{profile picture url}",
    "given_name": "{given name}",
    "family_name": "{family name}",
    "locale": "en",
    "iat": {iat},
    "exp": {exp}
  }
}
```

### Method: Logout

This references the base functionality of [Logout defined in GrandId API](/#grandid-api-methods-method-logout).

Currently there are no differences between oAuth and our generic API. This service does not log the user out of the IDP. It deletes the session from our systems.

## Errors

This references the base information of [Errors defined in GrandId API](/#grandid-api-errors).

Currently there are no differences between oAuth and our generic API except the following error: