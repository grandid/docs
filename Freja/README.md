# Freja

## Requirements

-   Full understanding of the functions in [GrandId API](/#a)
-   A working Freja ([test](https://frejaeid.com/testing/Freja%20eID%20Relying%20Party%20Documentation%20-%20Testing%20instructions.html#FrejaeIDRelyingPartyDocumentation-Testinginstructions-StartingtheFrejaeIDMobileApplicationinTestMode) or production) installation on your mobile device
-   If you wish to use [Freja Organisation eID](https://e-identitet.se/auth/e-legitimation/frejaorgeid/) we have dedicated documentation for this procedure which can be requested from info@e-identitet.se. The overall connecting procedure is done using the same parameters as all API calls to GrandID.

## Methods

Authenticating with Freja through us requires you to make two requests towards grandid.com.
The first step sends a request to Freja through us, thus receiving a token which will later be used to start a session on the clients' devices. This request will be sent once and towards this url

### Method: FederatedLogin

This references the base functionality of [FederatedLogin defined in GrandId API](/#grandid-api-methods-method-federatedlogin).

**Parameters** `/json1.1/FederatedLogin?apiKey={apiKey}&authenticateServiceKey={authenticateServiceKey}`

---

[POST]

#### Endpoint

| Environment     | Domain name                  |
|-----------------|------------------------------|
| Production (EU) | `client.grandid.com`         |
| Test (EU)       | `client-test.grandid.com`    |
| Production (SE) | `client.e-identitet.se`      |
| Test (EU)       | `client-test.e-identitet.se` |

#### URL parameters

| Request Parameters     | Type   | Default | Required | Comment                   |
| ---------------------- | ------ | ------- | -------- | ------------------------- |
| apiKey                 | string | -       | Yes      | The customer specific key |
| authenticateServiceKey | string | -       | Yes      | The service specific key  |

#### Body parameters

| Body Parameters      | Type                              | Default   | Required | Comment                                                                                                            |
| -------------------- | --------------------------------- | --------- | -------- | ------------------------------------------------------------------------------------------------------------------ |
| callbackUrl          | string, optionally base64-encoded | -         | Yes      | URL to send user back to together with the grandidsession-parameter                                                |
| personalNumber       | string                            | -         | No       | Swedish personal identifier, 12 digits, no dashes                                                                  |
| userVisibleData      | string, base64-encoded            | -         | No       | Mandatory (if signing), data to sign and show to the user                                                          |
| userNonVisibleData   | string, base64-encoded            | -         | No       | Optional data to sign which is not shown (otherwise userVisibleData is copied to this parameter)                   |
| signText             | string, base64-encoded            | -         | No       | Alias for userVisibleData, if both are set userVisibleData is used.                                                |
| signBinaryData       | string, base64-encoded            | -         | No       | Alias for userNonVisibleData, if both are set userNonVisibleData is used                                           |
| title                | string, base64-encoded            | SIGNERING | No       | Title to display in the signing view, above the normal text                                                        |
| userInfoType         | string                            | SSN       | No       | User info type, one of "PHONE", "EMAIL", "SSN", "CUST". By setting personalNumber this is automatically set to SSN. |
| userInfo             | string                            | -         | No       | User information corresponding to user info type. If "PHONE" must begin with +46 (country code)                    |
| countryCode          | string                            | `SE`      | No       | If userInfoType is set to SSN, countryCode can be set to a different country code than SE                          |
| minRegistrationLevel | string                            | `PLUS`    | No       | Minimum required registration level of a user (BASIC, EXTENDED, PLUS)                                              |
| pushNotification     | json, base64-encoded              | -         | No       | Text to display in push notification to user, base64 encoded json structure                                        |
| thisDevice           | boolean                           | -         | No       | Enable to allow the Freja eID application to be launched on the user's current device.                             |
| otherDevice          | boolean                           | -         | No       | Enable to allow the user to launch Freja eID manually on the correct device.                                       |
| deviceChoice         | boolean                           | -         | No       | Lets the user choose which device to launch Freja eID on. Takes priority over `thisDevice` and `otherDevice`.      |
| gui                  | boolean, true or false            | true      | No       | Sets the mode to no GUI when false. See [detailed instructions](/freja#freja-examples-freja-for-apps)              |
| qr                   | boolean, true or false            | true      | No       | Enables QR codes.                                                                 |
| customerURL          | string, base64-encoded            | -         | No       | Set this to display a link back to a page of your choice on some of our views, for the user to go back to the starting page.                       |
| enableAutoLaunch     | boolean, true or false            | false     | No       | Allows you to control whether the Freja eID will be launched automatically on the user's current device.           |

All parameters can be sent in the body with a POST or you can POST with the apiKey & authenticateServiceKey parameters as part of the URL. Historically callbackUrl has been sent in the URL which we strongly advise _against_. The body is sent as a standard form data.

#### Parameter: countryCode

`countryCode` Contains the ISO-3166 two-alphanumeric country code of the country where the SSN is issued and is only needed when userInfoType is set to SSN, and the person trying to authenticate/sign has an SSN issued in another country.

The default value is `SE`

#### Parameter: enableAutoLaunch

`enableAutoLaunch` depends on multiple factors that are taken into account for the context. This does not guarantee that the application will automatically be launched. When `enableAutoLaunch=true` an attempt will be made to launch the application automatically. Depending on the context automatic launch may not be possible.

The user will always be presented with a button that they can manually press to launch the application.

#### Response

Our api always returns a JSON response. If the request has been successfully processed you will receive the following response format:

```json
{
    "sessionId": "{sessionid}",
    "redirectUrl": "https://login.grandid.com/?sessionid={sessionid}"
}
```

The second step involves you using this session id to collect the information of that user's authentication.

### Method: GetSession

This references the base functionality of [GetSession defined in GrandId API](/#grandid-api-methods-method-getsession).

**Parameters** `/json1.1/GetSession?apiKey={apiKey}&authenticateServiceKey={authenticateServiceKey}&sessionId={sessionId}`

[GET]

| Request Parameters     | Type   | Default | Required | Comment                                                                                                 |
| ---------------------- | ------ | ------- | -------- | ------------------------------------------------------------------------------------------------------- |
| apiKey                 | string | -       | Yes      | The customer specific key                                                                               |
| authenticateServiceKey | string | -       | Yes      | The service specific key                                                                                |
| sessionId              | string | -       | Yes      | Session id returned from FederatedLogin or sent to the callbackUrl endpoint in "grandidsession" via GET |

The expected response for this request will be something like this:

```json
{
    "sessionId": "{sessionId}",
    "username": "{personalIdentifier}",
    "userAttributes": {
        "examplekey": "examplevalue",
        "...": "...",
        "...": "..."
    }
}
```

#### Example Freja Auth

```json
{
    "sessionId": "fd7b836cf87328c1ba02cc664a67f27d",
    "username": "201701012393",
    "userAttributes": {
        "status": "APPROVED",
        "userInfoType": "SSN",
        "userInfo": {
            "ssn": "201701012393",
            "country": "SE"
        },
        "timestamp": 1579250030844,
        "minRegistrationLevel": "PLUS",
        "requestedAttributes": {
            "basicUserInfo": {
                "name": "Namn",
                "surname": "Efternamn"
            },
            "ssn": {
                "ssn": "201701012393",
                "country": "SE"
            }
        },
        "authRef": "6Bl5pCHZF4k14As9gRie7_tXegxDyrG7liMVeiqYRYnzXz5YjHSDAdQYkFel6PMm"
    }
}
```

#### Example Freja Sign

```json
{
    "sessionId": "ca7b54370ee8989d63e6505567e7efd2",
    "username": "201701012393",
    "userAttributes": {
        "signRef": "-XIYqht4gA1Fm6......(not revealing the full string)",
        "status": "APPROVED",
        "timestamp": 1579167513781,
        "minRegistrationLevel": "PLUS",
        "signatureType": "EXTENDED",
        "signatureData": {
            "userSignature": "eyJraWQiOiI0QzNCREJFOE........(a long string)",
            "certificateStatus": "MIIT0QoBAKCCE8owghPGB........(a long string)"
        },
        "requestedAttributes": {
            "basicUserInfo": {
                "name": "Namn",
                "surname": "Efternamn"
            }
        },
        "userInfoType": "SSN",
        "userInfo": {
            "ssn": "201701012393",
            "country": "SE"
        }
    }
}
```

**DISCLAIMER**
The same results are not applicable if a standard service is not used or a standard service is modified .
The social security number displayed is not real.
Result generated on (2020-01-16).

### Method: Logout

This references the base functionality of [Logout defined in GrandId API](/#grandid-api-methods-method-logout).

**Parameters** `/json1.1/Logout?apiKey={apiKey}&authenticateServiceKey={authenticateServiceKey}&sessionId={sessionId}`

[GET]

| Request Parameters     | Type   | Default | Required | Comment                                                                                                 |
| ---------------------- | ------ | ------- | -------- | ------------------------------------------------------------------------------------------------------- |
| apiKey                 | string | -       | Yes      | The customer specific key                                                                               |
| authenticateServiceKey | string | -       | Yes      | The service specific key                                                                                |
| sessionId              | string | -       | Yes      | Session id returned from FederatedLogin or sent to the callbackUrl endpoint in "grandidsession" via GET |

The expected response for this request will be something like this:

```json
{
    "sessiondeleted": "1"
}
```

## Errors

When something goes wrong both during sending request and processing it, our api respondes with an error of this format:

```json
{
    "errorObject": {
        "code": "error code example",
        "message": "Information about the error"
    }
}
```

### Signing not allowed

```json
{
    "errorObject": {
        "message": "This service is not allowed to sign with Freja. Contact info@e-identitet.se",
        "code": "FREJA_SIGN_NOT_ALLOWED"
    }
}
```

## Examples

All the examples below can be used in the test environment by replacing the domain name `client.grandid.com` with `client-test.grandid.com`.

### Personal number login

> Freja login:
> To start a Freja session our API needs the personal number of the user

```bash
curl --request POST \
  --url 'https://client.grandid.com/json1.1/FederatedLogin?apiKey={apiKey}&authenticateServiceKey={authenticateServiceKey}' \
  --header 'content-type: application/x-www-form-urlencoded' \
  --data personalNumber=191111111111 \
  --data callbackUrl=https://yourdomain.com/callback
```

#### Response

Just like with authentication you will be returned a redirectUrl to send the user to.

```json
{
    "sessionId": "{sessionid}",
    "redirectUrl": "{redirectUrl}"
}
```

When the user is sent back your callbackUrl after successfully login, you retrieve the results via GetSession just like a normal login.

---

### Login with Email

> Freja login:
> To start a Freja session our api needs the ssn of the user

```bash
curl --request POST \
  --url 'https://client.grandid.com/json1.1/FederatedLogin?apiKey={apiKey}&authenticateServiceKey={authenticateServiceKey}' \
  --header 'content-type: application/x-www-form-urlencoded' \
  --data userInfoType=EMAIL \
  --data userInfo=userEmail@domain.com \
  --data callbackUrl=https://yourdomain.com/callback
```

#### Response

Just like with authentication you will be returned a redirectUrl to send the user to.

```json
{
    "sessionId": "{sessionid}",
    "redirectUrl": "{redirectUrl}"
}
```

When the user is sent back your callbackUrl after successfully login, you retrieve the results via GetSession just like a normal login.

### Signing

> Freja Signatures
> To start a Freja sign session our api needs a string to sign, base64 encoded. It can also handle all the other options of Freja login.
> You can either send the ssn as a parameter or use our GUI.

```bash
curl --request POST \
  --url 'https://client.grandid.com/json1.1/FederatedLogin?apiKey={apiKey}&authenticateServiceKey={authenticateServiceKey}' \
  --header 'content-type: application/x-www-form-urlencoded' \
  --data userNonVisibleData=VEVTVA== \
  --data userVisibleData=VEVTVA== \
  --data personalNumber=191111111111 \
  --data callbackUrl=https://localhost.com/example \
```

#### Response

Just like with authentication you will be returned a redirectUrl to send the user to.

```json
{
    "sessionId": "{sessionid}",
    "redirectUrl": "{redirectUrl}"
}
```

When the user is sent back your callbackUrl after successfully signing, you retrieve the results via GetSession just like a normal login.

### Signing with custom notification and custom title

> Freja Signatures
> To start a Freja sign session our api needs a string to sign, base64 encoded. It can also handle all the other options of Freja login.
> You can either send the ssn as a parameter or use our GUI.

```bash
curl --request POST \
  --url 'https://client.grandid.com/json1.1/FederatedLogin?apiKey={apiKey}&authenticateServiceKey={authenticateServiceKey}' \
  --header 'content-type: application/x-www-form-urlencoded' \
  --data userNonVisibleData=VEVTVA== \
  --data userVisibleData=VEVTVA== \
  --data personalNumber=191111111111 \
  --data callbackUrl=https://localhost.com/example \
  --data title=SEVMTE8= \
  --data pushNotification=eyAidGl0bGUiOiAiYSB0aXRsZSIsICJ0ZXh0IjogInNvbWUgdGV4dCIgfQ==
```

#### Response

Just like with authentication you will be returned a redirectUrl to send the user to.

```json
{
    "sessionId": "{sessionid}",
    "redirectUrl": "{redirectUrl}"
}
```

When the user is sent back your callbackUrl after successfully signing, you retrieve the results via GetSession just like a normal login.

### Freja for apps

> In order to implement Freja in native apps without redirecting the user to an external website, you can use our Freja for apps version
> To enable Freja for apps you can send "gui=false" as a post parameter to the FederatedLogin endpoint

The following example is suited for authentication implementations, signing is no different from the normal procedure,
send in a personalNumber and userVisibleData & userNonVisibleData to initiate it

```bash
curl --request POST \
  --url 'https://client.grandid.com/json1.1/FederatedLogin?apiKey={apiKey}&authenticateServiceKey={authenticateServiceKey}' \
  --header 'content-type: application/x-www-form-urlencoded' \
  --data gui=false \
  --data callbackUrl=https://localhost.com/example
```

#### Response

When gui is set to false, the response will have this structure:

```json
{
    "status": "RECEIVED",
    "sessionid": "{sessionId}",
    "authRef": "{auth token from Freja}",
    "qr": "{base64 presentation of an svg qr image}"
}
```

> To make debugging easier you can use the following web tool to convert base64 into a visual qr image: [Base64 to SVG](https://base64.guru/converter/decode/image/svg)

Notice the qr parameter in the response. You only receive it when you do not send a personalNumber.
QR is only enabled when personalNumber is not sent with the request.

#### Errors

> Error responses have the following structure:

```json
{
    "errorObject": {
        "code": "{error code}",
        "message": "{message}"
    }
}
```

Error codes are forwarded from Freja's api.

1. For a list of error codes and messages during the authentication process follow:
   [Freja Authentication Service Documentation](https://frejaeid.com/rest-api/Authentication%20Service.html)

2. For the signing process follow:
   [Freja Signature Service Documentation](https://frejaeid.com/rest-api/Signature%20Service.html)

#### Poll for results

> In order to get status updates, you need to poll from the following endpoint

```bash
curl --request GET \
  --url 'https://client.grandid.com/json1.1/GetSession?apiKey={apiKey}&authenticateServiceKey={authenticateServiceKey}&sessionid={sessionid}'
```

#### Poll response

> The first poll results follow the following structure

```json
{
    "statusObject": {
        "code": "{code}",
        "message": "{Message}"
    }
}
```

The status codes:

1. _STARTED_ -> Means the request has been received by Freja and awaiting for user to open their app
2. _DELIVERED_TO_MOBILE_ -> The user has been prompted for verification on their app
3. _CANCELED_ -> The user has declined the request
4. _EXPIRED_ -> The request has expired, start a new session

**Freja Organisation eID**
Everything above applies to Freja Organisation eID.
