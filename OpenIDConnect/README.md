# OpenID Connect

## Integrations

GrandID can solve different types of integrations implementing OpenID Connect.

### Third party identity provider

GrandID can connect to a third party identity provider that only supports OpenID Connect. Your client can connect to GrandID using any implementation you have such as API or SAML.

### Third party service provider (Proxy SP)

A third party wishes to access an identity on your identity provider, but the third party can only implement OpenID Connect.

GrandID can connect to your IdP using SAML for example, and get the identity, then forward the data to the requesting service provider over OpenID Connect.

### GrandID using OpenID Connect

You may also integrate with OpenID Connect directly towards GrandID, and let GrandID fetch the identity information.

## Endpoints

### Discovery

The Discovery or configuration endpoint will respond with a JSON payload containing the configuration possibilities.

#### Example discovery request

```shell
curl --request GET \
  --url https://oidc.grandid.com/v2/.well-known/openid-configuration
```

```shell
curl --request GET \
  --url https://oauth.grandid.com/.well-known/openid-configuration
```

### JSON Web Key Sets

Get all the JSON Web Key Sets that are allowed.

#### Example JWKS request

```shell
curl --request GET \
  --url https://oidc.grandid.com/v2/.well-known/jwks.json
```

```shell
curl --request GET \
  --url https://oauth.grandid.com/.well-known/jwks.json
```

### Authorize

Initiate the authorization for the user by redirecting them to the authorize endpoint.

#### Authorization Parameters

| Parameter               | Required/Optional | Description                                                                                                             |
|-------------------------|-------------------|-------------------------------------------------------------------------------------------------------------------------|
| `client_id`             | **Required**      | The Client ID provided to you for your application.                                                                     |
| `redirect_uri`          | **Required**      | Any of the allowed Redirect URIs added for your Client configuration.                                                   |
| `response_code`         | **Required**      | A response type from the configuration.                                                                                 |
| `grant_type`            | **Required**      | A supported grant type from the configuration.                                                                          |
| `scope`                 | **Required**      | A space separated list of scopes or claims. **MUST** include the `openid` scope. See supported scopes in configuration. |
| `nonce`                 | *Optional*        | A nonce provided by your application, which will be returned in the ID Token.                                           |
| `state`                 | *Optional*        | A state provided by your application, which will be returned in the ID Token.                                           |
| `code_challenge`        | *Optional*        | A code challenge passed by your application for the PKCE flow. **SHOULD** use the SHA-256 method.                       |
| `code_challenge_method` | *Optional*        | **Required** when `code_challenge` is passed. **SHOULD** be value `S256`.                                               |

#### Example Authorization request

Example request, with linebreaks only for visuals. All query parameters should be without linebreaks. Naturally each value should be updated for your request.

```shell
curl --request GET \
  --url 'https://oidc.grandid.com/v2/authorize
    ?client_id={CLIENT_ID}
    &redirect_uri=http%3A%2F%2Flocalhost
    &response_type=code
    &grant_type=authorization_code
    &scope=openid'
```

### Token

The token endpoint should be called with the authorization code, provided to the configured Redirect URI. The token endpoint will then respond with your id, access and refresh tokens. Decode the ID token to receive the requested attributes.

#### Example token request

```shell
curl --request POST \
  --url https://oidc.grandid.com/v2/access_token \
  --header 'Content-Type: multipart/form-data' \
  --form client_id={CLIENT_ID} \
  --form client_secret={CLIENT_SECRET} \
  --form grant_type=authorization_code \
  --form redirect_uri=http://localhost \
  --form code={AUTHORIZATION_CODE}
```

#### Example token response

```json
{
    "id_token": "{ID_TOKEN_JWT}",
    "token_type": "Bearer",
    "expires_in": 3600,
    "access_token": "{ACCESS_TOKEN_JWT}",
    "refresh_token": "{REFRESH_TOKEN}"
}
```

### User

You can fetch the user's attributes, from the claims by passing the Access token to the user endpoint.

#### Example user request

```shell
curl --request GET \
  --url https://oidc.grandid.com/v2/user \
  --header 'Authorization: Bearer {ACCESS_TOKEN_JWT}'
```

#### Example user response

Will contain the attributes from all the scopes specified in the *Authorize* request.

```json
{
    "foo": "FOO",
    "sub": "USERNAME"
}
```

### Logout

You may trigger a logout, invalidating the tokens by sending a request to the logout endpoint.

#### Example logout request

```shell
curl --request GET \
  --url 'https://oidc.grandid.com/v2/logout
  ?id_token_hint={ID_TOKEN_JWT}
  &post_logout_redirect_uri=http%3A%2F%2Flocalhost'
```

#### Example logout response

```json
{
    "time": 1686206249
}
```
