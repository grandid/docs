# SPAR - Statens personaddressregister

## Implementation

SPAR can be accessed via GrandID API, SAML or oAuth protocols. It will either be initiated automatically after a e-legitimation authentication (primarily BankiD or Freja) and added to the response. Thus the responses from services with SPAR added contain more data than a normal authentication.

## Requirements

* Full understanding of the functions in [GrandId API](/#a)
* A configurable oAuth IDP where our credentials can be added.

## FederateLogin

Just like with authentication you will be returned a redirectUrl to send the user to.

## GetSession

Just like with FederatedLogin, GetSession accepts these values to be sent both in the URL or in the POST BODY. This method's parameters can all successfully sent in the URL.

Some attributes from SPAR may not be included in the response, and you must be able to handle any attribute having a `null` value.

## SPAR Base Response

Specific customer example (returned attributes can change depending on user):

```json
{
    "sessionId": "7d61bcd8f6a698953e0b112b9f5c07eb",
    "username": "190001021234",
    "userAttributes": {
        "SPAR": {
          "spakoDatumFrom": "2018-03-06",
          "spakoDatumTill": "9999-12-31",
          "spakoUtdelningsadress2": "Street 1",
          "spakoPostNr": "12345",
          "spakoPostort": "CITY",
          "spakoFolkbokfordLanKod": "11",
          "spakoFolkbokfordKommunKod": "12",
          "spakoFolkbokforingsdatum": "2009-03-10",
          "spakoDistriktKod": "418348"
        }
    }
}
```

## SPAR Extended Response

This service is only available when ordered specifically from Svensk e-identitet. It contains amongst other things the property sekretressMarkering (protected identity).

Be advised this data is produced by [[SPAR]](https://statenspersonadressregister.se/) and is relayed as-is by Svensk e-identitet.
In certain cases data will be returned differently. The amount of addresses in particular.

To determine if an individual has received a flag of protected personal attributes the most appropriate way is:

1. Determine that the attribute SPAR.spakoSekretessmarkering exists.
2. Determine that the value of the above attribute is "N" for "Nej" (the user does not have a protected identity).

In all other cases, the response might indicate that the resulting dataset is returning limited information.

Further questions about the returned data can only be answered by reading the technical documentation at the above SPAR-link.

Specific customer example (returned attributes can change depending on user):

```json
{
    "sessionId": "7d61bcd8f6a698953e0b112b9f5c07eb",
    "username": "190001021234",
    "userAttributes": {
        "SPAR": {
            "status":"OK",
            "spakoPersonId":{
                "spakoFysiskPersonId":"19121212-1212"
                },
            "spakoSekretessmarkering":"N",
            "spakoSenasteAndringSPAR":"2018-03-06",
            "spakoPersondetaljer":{
                "spakoDatumFrom":"2009-03-11",
                "spakoDatumTill":"9999-12-31",
                "spakoFornamn":"Name",
                "spakoTilltalsnamn":"10",
                "spakoEfternamn":"Namesson",
                "spakoFodelsetid":"1912-12-12",
                "spakoKon":"M"
            },
            "spakoAdresser":[
                {
                    "spakoDatumFrom": "2018-03-06",
                    "spakoDatumTill": "9999-12-31",
                    "spakoUtdelningsadress2": "Street 1",
                    "spakoPostNr": "12345",
                    "spakoPostort": "CITY",
                    "spakoFolkbokfordLanKod": "11",
                    "spakoFolkbokfordKommunKod": "12",
                    "spakoFolkbokforingsdatum": "2009-03-10",
                    "spakoDistriktKod": "418348"
                },
                {
                    "spakoDatumFrom":"2016-01-05",
                    "spakoDatumTill":"2018-03-06",
                    "spakoUtdelningsadress2":"Street 1",
                    "spakoPostNr":"112 22",
                    "spakoPostort":"CITY",
                    "spakoFolkbokfordLanKod":"21",
                    "spakoFolkbokfordKommunKod": "82",
                    "spakoFolkbokforingsdatum":"2009-03-10",
                    "spakoDistriktKod":"418348"
                }
            ],
            "spakoNuvarandeAdress":{
                "spakoDatumFrom":"2018-03-06",
                "spakoDatumTill":"9999-12-31",
                "spakoUtdelningsadress2":"Street 1",
                "spakoPostNr":"112 21",
                "spakoPostort":"CITY",
                "spakoFolkbokfordLanKod":"21",
                "spakoFolkbokfordKommunKod":"82",
                "spakoFolkbokforingsdatum":"2009-03-10",
                "spakoDistriktKod":"418348"
            }
        }
    }
}
```

## SPAR med vårdnadshavarinformation

This service is intended to be used by for example private schools and apothecarys, it cannot be used unless your organization has a special license for this type of data from SPAR. The permit and contract is directly obtained from SPAR.
Svensk e-identitet have implemented support for this functionality to make handling of the communcation more streamlined and this service can be ordered from us, further instructions will be given upon request.

### Request Parameters

The following request parameters are available for the *SPAR med vårdnadshavarinformation* service.

| Request parameter                  | Type             | Example value                   | Required | Comment                                       |
|------------------------------------|------------------|---------------------------------|----------|-----------------------------------------------|
| `apiKey`                           | string, GET/BODY | `"my_api_key"`                  | Yes      | The customer specific key                     |
| `authenticateServiceKey`           | string, GET/BODY | `"my_authenticate_service_key"` | Yes      | The service specific key                      |
| `parentSSN`                        | string, GET/BODY | `"19011224XXXX"`                | Yes      | The personal number for the caretaker/parent. |
| `childSSN`                         | string, GET/BODY | `"20011224XXXX"`                | Yes      | The personal number of the child.             |

## SPAR med vårdnadshavarinformation Response

This is a example response of a request made towards our service:

```json
{
  "status": "COMPLETE",
  "code": "SUCCESS",
  "message": "The provided parent SSN matches with the child's SSN.",
  "sessionId": "729da645bxxxxx9cf49ecb",
  "isValidParent": true,
  "data": {
    "SPAR": {
      "status": "OK",
      "data": {
        "spakoPersonId": {
          "spakoFysiskPersonId": "YYYYMMDDXXXX"
        },
        "spakoSekretessmarkering": "N",
        "spakoSenasteAndringSPAR": "{yyyy-mm-dd}",
        "spakoPersondetaljer": {
          "spakoDatumFrom": "{yyyy-mm-dd}",
          "spakoDatumTill": "{yyyy-mm-dd}",
          "spakoFornamn": "{full name}",
          "spakoTilltalsnamn": "10",
          "spakoEfternamn": "{lastname}",
          "spakoFodelsetid": "{yyyy-mm-dd}",
          "spakoKon": "M"
        },
        "spakoRelation": [
          {
            "spakoDatumFrom": "{yyyy-mm-dd}",
            "spakoDatumTill": "{yyyy-mm-dd}",
            "spakoRelationstyp": "V",
            "spakoPersonId": {
              "spakoFysiskPersonId": "{parentssn}"
            }
          },
          {
            "spakoDatumFrom": "{yyyy-mm-dd}",
            "spakoDatumTill": "{yyyy-mm-dd}",
            "spakoRelationstyp": "V",
            "spakoPersonId": {
              "spakoFysiskPersonId": "{parentssn}"
            }
          }
        ],
        "spakoAdresser": [
          {
            "spakoDatumFrom": "{yyyy-mm-dd}",
            "spakoDatumTill": "{yyyy-mm-dd}",
            "spakoUtdelningsadress2": "{addressline}",
            "spakoPostNr": "{zipcode}",
            "spakoPostort": "{postal town}",
            "spakoFolkbokfordLanKod": "14",
            "spakoFolkbokfordKommunKod": "41",
            "spakoFolkbokforingsdatum": "{yyyy-mm-dd}",
            "spakoDistriktKod": "{digits}"
          },
          {
            "spakoDatumFrom": "{yyyy-mm-dd}",
            "spakoDatumTill": "{yyyy-mm-dd}",
            "spakoUtdelningsadress2": "{addressline}",
            "spakoPostNr": "{zipcode}",
            "spakoPostort": "{postal town}",
            "spakoFolkbokfordLanKod": "14",
            "spakoFolkbokfordKommunKod": "02",
            "spakoFolkbokforingsdatum": "{yyyy-mm-dd}",
            "spakoDistriktKod": "{digits}"
          }
        ],
        "spakoNuvarandeAdress": {
          "spakoDatumFrom": "{yyyy-mm-dd}",
          "spakoDatumTill": "{yyyy-mm-dd}",
          "spakoUtdelningsadress2": "{addressline}",
          "spakoPostNr": "{zipcode}",
          "spakoPostort": "{postal town}",
          "spakoFolkbokfordLanKod": "14",
          "spakoFolkbokfordKommunKod": "41",
          "spakoFolkbokforingsdatum": "{yyyy-mm-dd}",
          "spakoDistriktKod": "{digits}"
        }
      }
    }
  }
}```
