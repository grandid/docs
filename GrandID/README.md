# GrandID API

## Implementation

![GrandID overview architecture](https://cdn.grandid.com/docs/grandid_flow.png "GrandID overview for a authentication")

## Requirements

```text
- apiKey - which you will receive once you have signed a contract with Svensk E-Identitet
- authenticateServiceKey
- Basic knowledge about REST APIs is required in order to understand the procedure and usage of this documentation.
```
## Services supported
See more on our specific services at our webpage, [e-identitet.se](https://e-identitet.se/tjanster).
Most of the services utilises the basics of the simple GrandID REST API. Some services require or have optional parameters. They are either documented here or sent together with your keys when signing our agreement.

## Endpoints
GrandID has a production and a test environment. The API calls are the same in both, only the domain name changes: `client.grandid.com` for production and `client-test.grandid.com` for test.

| Environment     | Client service URL                                                   | Login service URL                                                   |
|-----------------|----------------------------------------------------------------------|---------------------------------------------------------------------|
| Production (EU) | [https://client.grandid.com/](https://client.grandid.com/)           | [https://login.grandid.com/](https://client.grandid.com/)           |
| Test (EU)       | [https://client-test.grandid.com/](https://client-test.grandid.com/) | [https://login-test.grandid.com/](https://client-test.grandid.com/) |
| Production (SE) | <https://client.e-identitet.se>                                      | <https://login.e-identitet.se>                                      |
| Test (SE)       | <https://client.test.e-identitet.se>                                 | <https://login.test.e-identitet.se>                                 |
## Methods

Authenticating with GrandID requires you to make two requests towards grandid.com.
The first request initializes the session, the second request gathers the result from us. 

### Method: FederatedLogin

**Parameters** `/json1.1/FederatedLogin?apiKey={apiKey}&authenticateServiceKey={authenticateServiceKey}`

[POST]

| Request Parameters                 | Type                                | Default | Required | Comment                                                                                                                                   |
|------------------------------------|-------------------------------------|---------|----------|-------------------------------------------------------------------------------------------------------------------------------------------|
| apiKey                             | string                              | -       | Yes      | The customer specific key                                                                                                                 |
| authenticateServiceKey             | string                              | -       | Yes      | The service specific key                                                                                                                  |
| callbackUrl                        | string                              | -       | Yes      | The URL which the user is redirected back to after authentication. "grandidsession" will be passed along for later use.                  |

**Response**
Our api always returns a JSON response. If the request has been successfully processed you will receive the following response format:

```json
{
    "sessionId": "{sessionid}",
    "redirectUrl": "https://login.grandid.com/?sessionid={sessionid}"
}
```

* * *

The second step involves you using this session id to collect the information of that user's authentication.

### Method: GetSession

**Parameters** `/json1.1/GetSession?apiKey={apiKey}&authenticateServiceKey={authenticateServiceKey}&sessionId={sessionId}`


[GET]

| Request Parameters                 | Type                                | Default | Required | Comment                                                                                                                                   |
|------------------------------------|-------------------------------------|---------|----------|-------------------------------------------------------------------------------------------------------------------------------------------|
| apiKey                             | string                              | -       | Yes      | The customer specific key                                                                                                                 |
| authenticateServiceKey             | string                              | -       | Yes      | The service specific key                                                                                                                  |
| sessionId                          | string                              | -       | Yes      | The session id retrieved from FederatedLogin or GET-parameter grandidsession (sent by us to you)                                          |

**Response**

The expected response for this request will be something like this:

```json
{
    "sessionId": "{sessionId}",
    "username": "{SSN}",
    "userAttributes": {
        "examplekey": "examplevalue",
        "...": "...",
        "...": "..."
    }
}
```

### Method: Logout

**Parameters** `/json1.1/Logout?apiKey={apiKey}&authenticateServiceKey={authenticateServiceKey}&sessionId={sessionId}`

[GET]

| Request Parameters                 | Type                                | Default | Required | Comment                                                                                                                                   |
|------------------------------------|-------------------------------------|---------|----------|-------------------------------------------------------------------------------------------------------------------------------------------|
| apiKey                             | string                              | -       | Yes      | The customer specific key                                                                                                                 |
| authenticateServiceKey             | string                              | -       | Yes      | The service specific key                                                                                                                  |
| sessionId                          | string                              | -       | Yes      | The session id retrieved from FederatedLogin or GET-parameter grandidsession (sent by us to you)                                          |

**Response**

The expected response for this request will be something like this:

```json
{
    "sessiondeleted": "1"
}
```

## Errors

When something goes wrong both during sending request and processing it, our api respondes with an error of this format:

```json
{
    "errorObject": {
        "code": "error code example",
        "message": "Information about the error"
    }
}
```

## Examples

The examples below use the production environment (`login.grandid.com` and `client.grandid.com`), but it is also possible to send requests to the test environment (`login-test.grandid.com` and `client-test.grandid.com`).

### FederatedLogin

> Initializing a GrandID service

| Request parameter                  | Type                                | Value (Default)                    | Required | Comment                                                                                                                                  |
|------------------------------------|-------------------------------------|------------------------------------|----------|------------------------------------------------------------------------------------------------------------------------------------------|
| Content-Type                       | header                              | application/x-www-form-urlencoded  | Yes      | The type of request being made                                                                                                           |
| Endpoint                           | The service answering               | https://login.grandid.com/json1.1/ | Yes      | The URL of the request (concatenated with Method below)                                                                                  |
| Method                             | The function being called           | FederatedLogin                     | Yes      | GrandID have several methods, most commonly FederatedLogin, GetSession and Logout                                                        |
| apiKey                             | string, GET/BODY                    | your apiKey                        | Yes      | The customer specific key                                                                                                                |
| authenticateServiceKey             | string, GET/BODY                    | your authenticateServiceKey        | Yes      | The service specific key                                                                                                                 |
| callbackUrl                        | string, BODY                        | your callback url                  | Yes      | It is possible to send the callbackUrl as a parameter directly in the URL, but it's something we strongly advise against                 |


**CURL Example**

```bash
curl --request POST \
  --url 'https://client.grandid.com/json1.1/FederatedLogin?apiKey=apiKey&authenticateServiceKey=authenticateServiceKey' \
  --header 'content-type: application/x-www-form-urlencoded' \
  --data callbackUrl=https://localhost/example
```

### GetSession

> Poll GetSession to get user status / attributes

| Request parameter                  | Type                                | Value (Default)                    | Required | Comment                                                                                                                                  |
|------------------------------------|-------------------------------------|------------------------------------|----------|------------------------------------------------------------------------------------------------------------------------------------------|
| Content-Type                       | header                              | application/x-www-form-urlencoded  | Yes      | The type of request being made                                                                                                           |
| Endpoint                           | The service answering               | https://login.grandid.com/json1.1/ | Yes      | The URL of the request (concatenated with Method below)                                                                                  |
| Method                             | The function being called           | GetSession                         | Yes      | GrandID have several methods, most commonly FederatedLogin, GetSession and Logout                                                        |
| apiKey                             | string, GET/BODY                    | your apiKey                        | Yes      | The customer specific key                                                                                                                |
| authenticateServiceKey             | string, GET/BODY                    | your authenticateServiceKey        | Yes      | The service specific key                                                                                                                 |
| sessionid                          | string, BODY                        | sessionid                          | Yes      | The session id you either retrieve via grandidsession on your callbackUrl endpoint, or have saved from your FederatedLogin-request       |


**CURL Example**

```bash
curl --request GET \
  --url 'https://client.grandid.com/json1.1/GetSession?apiKey=apiKey&authenticateServiceKey=authenticateServiceKey&sessionid=sessionid'
```

### Logout

> Delete all data about the user from GrandID

| Request parameter                  | Type                                | Value (Default)                    | Required | Comment                                                                                                                                  |
|------------------------------------|-------------------------------------|------------------------------------|----------|------------------------------------------------------------------------------------------------------------------------------------------|
| Content-Type                       | header                              | application/x-www-form-urlencoded  | Yes      | The type of request being made                                                                                                           |
| Endpoint                           | The service answering               | https://login.grandid.com/json1.1/ | Yes      | The URL of the request (concatenated with Method below)                                                                                  |
| Method                             | The function being called           | Logout                             | Yes      | GrandID have several methods, most commonly FederatedLogin, GetSession and Logout                                                        |
| apiKey                             | string, GET/BODY                    | your apiKey                        | Yes      | The customer specific key                                                                                                                |
| authenticateServiceKey             | string, GET/BODY                    | your authenticateServiceKey        | Yes      | The service specific key                                                                                                                 |
| sessionid                          | string, BODY                        | sessionid                          | Yes      | The session id you either retrieve via grandidsession on your callbackUrl endpoint, or have saved from your FederatedLogin-request       |


**CURL Example**

```bash
curl --request GET \
  --url 'https://client.grandid.com/json1.1/Logout?apiKey=apiKey&authenticateServiceKey=authenticateServiceKey&sessionid=sessionid'
```

## Links

**Code examples**

*  [Reference composer package from Svensk e-identitet](https://bitbucket.org/grandid/bankidv3/src/master/)

**Tools**

*  [Insomnia](https://insomnia.rest)
*  [Postman](https://www.getpostman.com/)

Usage of REST tools is strongly encouraged while developing against APIs. The time invested in setting them up is well worth the return in shortened development time.
  
**Example configurations**

*  [Insomnia example](/download?file=insomnia.json)
*  [Postman example](/download?file=postman.json)
