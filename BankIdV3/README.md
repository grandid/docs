# BankID Documentation

This is the GrandID API documentation for our BankID services.

Before you proceed you should make sure you have a good understanding of [GrandID API's structure](/). References to
the generic API may be made without further explanation.

You should also make sure you have a valid BankID identity issued to one of your devices. You can issue a test-identity
through <https://demo.bankid.com/>, make sure it's being used with the correct service.

**We strongly recommend our customers to NOT use BankID in an iFrame solution.**

# Generic parameters

These are the generic parameters, that applies to all endpoints.

## API Key

- Name: `apiKey`
- Required? **Yes**
- Type: `string`

The API Key is a parameter that is provided to you for authentication against our systems. This parameter **MUST** be
passed with your requests.

## Authenticate Service Key

- Name: `authenticateServiceKey`
- Required: **Yes**
- Type: `string`

The authenticate service key identifies which service should be executed for you. This parameter **MUST** be passed
with your requests.

# FederatedLogin

This endpoint initiates the entire authentication or signature process. Depending on the service's internal
configuration, and your request parameters the response may differ.

- Accepted methods: `POST`
- Endpoint: `/json1.1/FederatedLogin`

Parameters **SHOULD** be POSTed as a multipart form, with a `Content-Type: multipart/form-data` header.

## All parameters

All parameters with their data type, whether they're required and a simple description. Please see each parameter's own heading for more detailed information.

| Name                     | Type     | Required?    | Description                                                                                 |
|--------------------------|----------|--------------|---------------------------------------------------------------------------------------------|
| `apiKey`                 | `string` | **Required** | The API key provided to you by us.                                                          |
| `authenticateServiceKey` | `string` | **Required** | The service key provided to you by us.                                                      |
| `callbackUrl`            | `string` | _Optional_   | Where to return end-user after completion.                                                  |
| `customerURL`            | `string` | _Optional_   | Where to return end-user if they press the back button.                                     |
|                          |          |              |                                                                                             |
| `userVisibleData`        | `base64` | _Optional_   | Visible data for the end-user to sign.                                                      |
| `userNonVisibleData`     | `base64` | _Optional_   | Hidden data included in the signature.                                                      |
| `userVisibleDataFormat`  | `string` | _Optional_   | Used to format the visible signature data.                                                  |
| `authMessage`            | `base64` | _Optional_   | Visible data for the end-user to auth.                                                      |
|                          |          |              |                                                                                             |
| `mobileBankId`           | `bool`   | _Optional_   | Set to `true` to force usage of a Mobile BankID.                                            |
| `desktopBankId`          | `bool`   | _Optional_   | Set to `true` to force usage of a Desktop BankID.                                           |
| `thisDevice`             | `bool`   | _Optional_   | Set to `true` to allow usage of the end-users current device                                |
| `qr`                     | `bool`   | _Optional_   | Set to `true` to allow authentication/signing using a QR code                               |
|                          |          |              |                                                                                             |
| `allowFingerprintAuth`   | `string` | _Optional_   | Set whether usage of fingerprint biometrics is allowed with the authentication.             |
| `allowFingerprintSign`   | `string` | _Optional_   | Set whether usage of fingerprint biometrics is allowed with the signature.                  |
|                          |          |              |                                                                                             |
| `gui`                    | `string` | _Optional_   | Set to `false` to opt out of GrandID's user interface and build your custom implementation. |
| `appRedirect`            | `string` | _Optional_   | Can be used to force a redirect to specific application from the BankID application.        |

## Callback URL

- Name: `callbackUrl`
- Required: **No**
- Type: `string`

The callback URL **SHOULD** be passed with the request, but there are cases where it may be unnecessary.

Generally the end-user will be redirected from the GrandID services to the specified callback URL after a completed
session.

The callback URL may likely be a URL within your own systems, such as `http://localhost/auth/callback`. The end-user
will get redirected with an extra query parameter including the SessionID they had, such
as `http://localhost/auth/callback?grandidsession={SESSION_ID}`.

## Customer URL

- Name: `customerURL`
- Required: **No**
- Type: `string`

The customer URL **MAY** be passed with the request.

If a customer URL is passed a back button will appear during the BankID login. 
If this back button is pressed the end-user will be redirected from the GrandId service to the specified URL passed in the customer URL parameters.

## Authentication

Parameters related to performing authentication.

### Visible authorization data

- Name `authMessage`
- Required: **No**
- Type: `base64-encoded-string`

The `authMessage` parameter can be used similar to `userVisibleData`, to display a visible text to the end-user during authentication.

## Signatures

Parameters related to performing signatures instead of authentications. These are triggered by passing
the `userVisibleData` parameter according to below.

Signatures are enabled on a per-service basis, so make sure you use the correct _authenticate service key_ when
attempting to sign.

### Visible signature data

- Name: `userVisibleData`
- Required: **No**
- Type: `base64-encoded-string`

To initiate a signature you **MUST** pass the `userVisibleData` parameter, as a Base64-Encoded string. The encoded data
**MUST NOT** exceed 40000 bytes.

This is the data that will be visible to the end-user in their BankID application during signature. The data can be
stylized, see `userVisibleDataFormat` parameter.

### Invisible signature data

- Name: `userNonVisibleData`
- Required: **No**
- Type: `base64-encoded-string`

You **MAY** pass extra, invisible signature data with the `userNonVisibleData` parameter. This data **MUST NOT** exceed
200000 bytes.

To be able to pass `userNonVisibleData` you **MUST** also pass the `userVisibleData`, see its documentation for more
information.

### Visible signature data format

- Name: `userVisibleDataFormat`
- Required: **No**
- Type: `string`

Can be set to `simpleMarkdownV1` to use Markdown formatting with the `userVisibleData` parameter.
See <https://www.bankid.com/utvecklare/guider/formatera-text/formatera-text-introduktion> for more information.

## BankID Types

There are two main different types of BankID, as defined by their certificates.

For information these are also separated by production and test, which is configured on a per-service basis.
See <https://demo.bankid.com> to issue a test-identity for yourself.

Make sure you're using the correct type of identity with your service.

For clarity to yourselves it is recommended that both the `mobileBankId` and `desktopBankId` parameter are passed. This
will make it more clear for yourself what your intended behavior is.

Passing both parameter as `false` is strongly discouraged.

### Mobile BankID

- Name: `mobileBankId`
- Required: **No**
- Type: `bool`

You can enforce the end-user to authenticate or sign using a Mobile BankID by passing `mobileBankId=true`.

Passing `mobileBankId=true` implicitly sets `desktopBankId=false`.

### Desktop BankID

- Name: `desktopBankId`
- Required: **No**
- Type: `bool`

You can enforce the end-user to authenticate or sign using a Desktop BankID by passing `desktopBankId=true`.

Passing `desktopBankId=true` implicitly sets `mobileBankId=false`.

### This Device

- Name: `thisDevice`
- Required: **No**
- Type: `bool`

You can allow the end-user to authenticate or sign using the end-users current device `thisDevice=true`.

## QR

- Name: `qr`
- Required: **No**
- Type: `bool`

In order to allow authentication/signing from another device you can set `qr=true` to enable QR codes.

## Fingerprints

It's possible to control whether users are allowed to use fingerprints to complete their authentication or signature.
It's possible to toggle this independently.

### Authentications

- Name: `allowFingerprintAuth`
- Required: **No**
- Type: `bool`

Set this value to control whether the end-user is allowed to complete the authentication using fingerprints. If the
user doesn't have a fingerprint set up or enabled, the application will fall back to let the user use their PIN.

### Signatures

- Name: `allowFingerprintSign`
- Required: **No**
- Type: `bool`

Set this value to control whether the end-user is allowed to complete the signature using fingerprints. If the user
doesn't have a fingerprint set up or enabled, the application will fall back to let the user use their PIN.

## Custom behavior

There are some parameters if you need more custom handling of either interfaces, or BankID application behavior.

When using custom behavior you **SHOULD** read the [Mobile Integrations](/mobileintegrations) documentation as well.

### Toggle GrandID Interface

- Name: `gui`
- Required: **No**
- Type: `bool`

You can opt out of using GrandID's Interfaces by passing `gui=false`, and logic and instead otp to build your own
custom interface implementation.

See BankID's documentation regarding application launch <https://www.bankid.com/en/utvecklare/guider/teknisk-integrationsguide/programstart>.

### Application redirection

- Name: `appRedirect`
- Required: **No**
- Type: `string`

You can force the BankID application to handle redirection after successful authentication or signature in custom ways
with the `appRedirect` parameter.

By default, the BankID application will try to redirect to the previous application. For iOS devices this may be a manual process where the end user must press a back button.

Your implementation **MAY** pass a custom `appRedirect` instruction to where the BankID application will redirect the end user.

It is **RECOMMENDED** that the redirect instruction is a URL starting with `https://`. However, it appears that custom url schemes may work.

# GetSession

This endpoint should get called by your application from the specified callback URL in the FederatedLogin request. It
collects the user attributes and responds with them.

Services that are using a custom interface implementation needs to poll this endpoint repeatedly instead.

- Accepted methods: `GET`, `POST`
- Endpoint: `/json1.1/GetSession`

## Session ID parameter

- Name: `sessionId`
- Required: **Yes**
- Type: `string`

Aside from passing the *generic parameters* as documented above, the `sessionId` **MUST** also be included.
This `sessionId` will be provided to the callback URL in the `grandidsession` query parameter.

## Custom interface implementations

- Your implementation **MUST** repeatedly poll this endpoint until session is complete.
- Your implementation **MUST NOT** poll this more often than every 2 seconds.
- Your implementation **SHOULD** update its QR image after each poll, if using QR Codes.

# Logout

Please read the basic behavior in [generic GrandID API](/#grandid-api-methods-method-logout).

- Accepted methods: `GET`
- Endpoint: `/json1.1/Logout`

## Cancel BankID authentication/signature

- Name: `cancelBankID`
- Required: **No**
- Type: `bool`

Pass `cancelBankID=true` to cancel any ongoing BankID transactions.

# Integrations

## Custom integrations

When you've opted to build your custom integration, using `gui=false`, you'll be responsible for launching the BankID application on the user's device as necessary.

In short, that means you'll most likely be using one of the following URLs to launch the application from a browser, depending on the user's device.

- Android/iOS: `https://app.bankid.com/?autostarttoken={AUTO_START_TOKEN}&redirect=null`
- Desktop devices: `bankid:///?autostarttoken={AUTO_START_TOKEN}&redirect=null`

**Note**: these are the generic recommendations for simple cases where the application is being launched from the user's browser.

See also [BankID's Documentation on Launching](https://www.bankid.com/en/utvecklare/guider/teknisk-integrationsguide/programstart).

## BankID with iFrame

We strongly advise you not to use BankID in **iFrame**. Even though there are ways to use it we cannot guarantee that BankID will work in every browser and every device. Modern browsers like Chrome for example enforce by default **same-origin policy** in order to prevent **Cross-site scripting attacks (XSS)** (Read more at [https://developers.google.com/web/fundamentals/security/csp](https://developers.google.com/web/fundamentals/security/csp))  **iFrame** provides the injection of content from pages of different origin which violates the **same-origin policy** There are workarounds (i.e Use of whitelists) for every browser and platform, but it is difficult to provide and ensure universal compatibility. Additionally, the management of cookies used by BankID becomes more complicated when using **iFrame**.

# Examples

Below you can see an example cURL request including every single parameter.

## Simple authentication

For a simple authentication request, without any additional data and relying on the internal configuration it would look
something like this.

### Request

```shell
curl --request POST \
  --url https://client.grandid.com/json1.1/FederatedLogin \
  --header 'Content-Type: multipart/form-data' \
  --form apiKey=BANKID_API_BAR \
  --form authenticateServiceKey=BANKID_SERVICE_FOO \
  --form callbackUrl=http://localhost/auth/callback
```

### Response

When the service is configured to use GrandID's Interfaces this is what will be returned in the response.

```json
{
    "sessionId":"SESSION_ID",
    "redirectUrl":"https://login.grandid.com?sessionid=SESSION_ID"
}
```

## Disable GrandID Interface

Example request showing how to disable GrandID's Interface, allowing for a custom implementation. This includes an
explicit parameter of `qr=true`.

```shell
curl --request POST \
  --url https://client.grandid.com/json1.1/FederatedLogin \
  --header 'Content-Type: multipart/form-data' \
  --form apiKey=BANKID_API_BAR \
  --form authenticateServiceKey=BANKID_SERVICE_FOO \
  --form callbackUrl=http://localhost/auth/callback \
  --form gui=false \
  --form qr=true
```

If your service is configured to implement a custom interface (see `gui` parameter), this is what the response looks
like.

**Note** that the `QRCode` property will not be present if the `qr` parameter is disabled.

```json
{
    "sessionId":"SESSION_ID",
    "QRCode":"BASE64_QR_CODE_DATA",
    "autoStartToken":"AUTO_START_TOKEN"
}
```

## Basic GetSession

How to perform a GetSession request.

### Request

```shell
curl --request POST \
  --url 'https://client.grandid.com/json1.1/GetSession' \
  --header 'Content-Type: multipart/form-data' \
  --form apiKey=BANKID_API_BAR \
  --form authenticateServiceKey=BANKID_SERVICE_FOO \
  --form sessionId=SESSION_ID
```

### Responses

There are multiple different forms of responses from the GetSession API endpoint.

#### Not logged in

The session hasn't been completed and not yet considered valid, this is a very generic message.

```json
{
    "errorObject": {
        "code": "NOTLOGGEDIN",
        "message": "This session SESSION_ID is not a logged in user"
    }
}
```

#### Custom interface implementations

When having requested using a custom interface implementation with `gui=false` to the FederatedLogin endpoint, the
GetSession endpoint will respond like below during the polling procedure. Note that the `status` and `hintCode`
properties will change during the process.

**Note** that the `QRCode` property depends on whether the `qr` option is enabled. It will be omitted if `qr=false`.

##### Outstanding transaction

Transaction was recently initiated and is waiting for the user to launch their BankID application.

```json
{
    "grandidObject": {
        "code": "BANKID_MSG",
        "message": {
            "status": "pending",
            "hintCode": "outstandingTransaction"
        },
        "sessionId": "SESSION_ID",
        "QRCode": "BASE64_QR_CODE_DATA",
        "autoStartToken": "AUTO_START_TOKEN"
    }
}
```

##### User signing

When the user has started inputting their PIN in their BankID application.

```json
{
    "grandidObject": {
        "code": "BANKID_MSG",
        "message": {
            "status": "pending",
            "hintCode": "userSign"
        },
        "sessionId": "SESSION_ID",
        "QRCode": "QR_CODE",
        "autoStartToken": "AUTO_START_TOKEN"
    }
}
```

##### Start failed

User didn't launch their BankID application in time.

```json
{
    "grandidObject": {
        "code": "BANKID_MSG",
        "message": {
            "status": "failed",
            "hintCode":"startFailed",
        },
        "sessionId": "SESSION_ID",
        "autoStartToken": "AUTO_START_TOKEN"
    }
}
```

#### Completed authentication/signature

When the authentication or signature is successfully completed, you'll get the following response.

```json
{
    "sessionId": "SESSION_ID",
    "username": "YYYYMMDDXXXX",
    "userAttributes": {
        "personalNumber": "YYYYMMDDXXXX",
        "name": "FULL_NAME",
        "givenName": "GIVEN_NAME",
        "surname": "SUR_NAME",
        "ipAddress": "127.0.0.1",
        "notBefore": "UNIX_TIMESTAMP_START_MILLISECONDS",
        "notAfter": "UNIX_TIMESTAMP_END_MILLISECONDS",
        "signature": "BASE64_SIGNATURE",
        "ocspResponse": "OCSP_RESPONSE",
        "uhi": "UNIQUE_HARDWARE_IDENTIFIER",
        "bankIdIssueDate": "YYYY-MM-DDZ"
    }
}
```

#### User canceled transaction

The user canceled the BankID authentication or signature.

```json
{
    "grandidObject": {
        "code": "BANKID_MSG",
        "message": {
            "status": "failed",
            "hintCode": "userCancel"
        },
        "sessionId": "SESSION_ID",
        "QRCode": "BASE64_QR_CODE_DATA",
        "autoStartToken": "AUTO_START_TOKEN"
  }
}
```

# Errors

There are a bunch of errors that can be returned from the API. You should check the response body for
the `errorObject.code` property. See example structure below.

```json
{
    "errorObject": {
        "code": "ERROR_CODE",
        "message": "HUMAN_READABLE_ERROR_MESSAGE"
    }
}
```

Below you can find a set of error codes, their cause and attempted fix.

## Incorrect Callback URL

- Code: `INCORRECT_URL_DATA`
- Description: You haven't passed a correctly formatted `callbackURL` parameter (see its documentation above).
- Remediation: Ensure your URL is either in proper URL format such as `https://example.org/callback`.

## Incorrect Customer URL

- Code: `INCORRECT_CUSTOMER_URL_DATA`
- Description: You haven't passed a correctly formatted `customerURL` parameter (see its documentation above).
- Remediation: Ensure your URL is either in proper URL format such as `https://example.org/login`.

## Authentications not allowed

- Code: `BANKID_AUTH_NOT_ALLOWED`
- Description: The service is not configured to allow authentications.
- Remediation: If your intention was to initiate a signature see documentation for `userVisibleData` parameter,
  otherwise contact us.

## Signatures not allowed

- Code: `BANKID_SIGN_NOT_ALLOWED`
- Description: The service is not configured to allow signatures.
- Remediation: Either stop passing the `userVisibleData` parameter to authenticate instead of sign, or contact us to
  enable signatures.

## Incorrect signature data format

- Code: `BANKID_INCORRECT_DATA`
- Description: The `userVisibleData` passed is not properly Base64 encoded.
- Remediation: Ensure that your `userVisibleData` parameter is correct Base64 data.

## Size of user visible data too large

- Code: `BANKID_SIGN_USER_VISIBLE_SIZE`
- Description: The `userVisibleData` exceeded its size limit.
- Remediation: Reduce the amount of data.

## Incorrect signature data

- Code: `INCORRECT_SIGN_DATA`
- Description: The `userVisibleData` hasn't been properly received for the signature.
- Remediation: Ensure that both `userVisibleData` and `userNonVisibleData` are passed according to the documentation.

## Incorrect hidden signature data

- Code: `BANKID_INCORRECT_DATA`
- Description: The `userNonVisibleData` passed is not properly Base64 encoded.
- Remediation: Ensure that your `userNonVisibleData` parameter is correct Base64 data.

## Size of user non visible data too large

- Code: `BANKID_SIGN_USER_NONVISIBLE_SIZE`
- Description: The `userNonVisibleData` exceeded its size limit.
- Remediation: Reduce the amount of data.

## Invalid certificate policy

- Code: `INVALID_CERTIFICATE_POLICY`
- Description: Both types of BankID identities have been disabled.
- Remediation: Make sure either `mobileBankId=true` or `desktopBankId=true`.

## Insufficient data for meaningful answer

- Code: `INSUFFICIENT_DATA_FOR_MEANINGFUL_ANSWER`
- Description: Every way for the end-user to interact with the BankID application has been disabled.
- Remediation: You should pass either `thisDevice`, or `qr` equal to `true` to allow some interaction.